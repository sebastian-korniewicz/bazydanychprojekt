﻿namespace wypozyczalnia
{
    partial class EndHire
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonEndHire = new System.Windows.Forms.Button();
            this.labelChoosenData = new System.Windows.Forms.Label();
            this.dataGridViewChoosenData = new System.Windows.Forms.DataGridView();
            this.labelClientLastNAme = new System.Windows.Forms.Label();
            this.labelClientName = new System.Windows.Forms.Label();
            this.textBoxClientLastName = new System.Windows.Forms.TextBox();
            this.textBoxClientName = new System.Windows.Forms.TextBox();
            this.dataGridViewHires = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewChoosenData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHires)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonEndHire
            // 
            this.buttonEndHire.Location = new System.Drawing.Point(871, 557);
            this.buttonEndHire.Name = "buttonEndHire";
            this.buttonEndHire.Size = new System.Drawing.Size(132, 51);
            this.buttonEndHire.TabIndex = 23;
            this.buttonEndHire.Text = "Zakończ wypożyczenie";
            this.buttonEndHire.UseVisualStyleBackColor = true;
            this.buttonEndHire.Click += new System.EventHandler(this.buttonEndHire_Click);
            // 
            // labelChoosenData
            // 
            this.labelChoosenData.AutoSize = true;
            this.labelChoosenData.Location = new System.Drawing.Point(12, 448);
            this.labelChoosenData.Name = "labelChoosenData";
            this.labelChoosenData.Size = new System.Drawing.Size(69, 17);
            this.labelChoosenData.TabIndex = 22;
            this.labelChoosenData.Text = "Wybrano:";
            // 
            // dataGridViewChoosenData
            // 
            this.dataGridViewChoosenData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewChoosenData.Location = new System.Drawing.Point(12, 477);
            this.dataGridViewChoosenData.Name = "dataGridViewChoosenData";
            this.dataGridViewChoosenData.RowTemplate.Height = 24;
            this.dataGridViewChoosenData.Size = new System.Drawing.Size(991, 72);
            this.dataGridViewChoosenData.TabIndex = 21;
            // 
            // labelClientLastNAme
            // 
            this.labelClientLastNAme.AutoSize = true;
            this.labelClientLastNAme.Location = new System.Drawing.Point(72, 51);
            this.labelClientLastNAme.Name = "labelClientLastNAme";
            this.labelClientLastNAme.Size = new System.Drawing.Size(67, 17);
            this.labelClientLastNAme.TabIndex = 18;
            this.labelClientLastNAme.Text = "Nazwisko";
            // 
            // labelClientName
            // 
            this.labelClientName.AutoSize = true;
            this.labelClientName.Location = new System.Drawing.Point(72, 26);
            this.labelClientName.Name = "labelClientName";
            this.labelClientName.Size = new System.Drawing.Size(33, 17);
            this.labelClientName.TabIndex = 17;
            this.labelClientName.Text = "Imie";
            // 
            // textBoxClientLastName
            // 
            this.textBoxClientLastName.Location = new System.Drawing.Point(159, 51);
            this.textBoxClientLastName.Name = "textBoxClientLastName";
            this.textBoxClientLastName.Size = new System.Drawing.Size(200, 22);
            this.textBoxClientLastName.TabIndex = 15;
            this.textBoxClientLastName.TextChanged += new System.EventHandler(this.textBoxClientLastName_TextChanged);
            // 
            // textBoxClientName
            // 
            this.textBoxClientName.Location = new System.Drawing.Point(159, 23);
            this.textBoxClientName.Name = "textBoxClientName";
            this.textBoxClientName.Size = new System.Drawing.Size(200, 22);
            this.textBoxClientName.TabIndex = 14;
            this.textBoxClientName.TextChanged += new System.EventHandler(this.textBoxClientName_TextChanged);
            // 
            // dataGridViewHires
            // 
            this.dataGridViewHires.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewHires.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewHires.Location = new System.Drawing.Point(12, 88);
            this.dataGridViewHires.Name = "dataGridViewHires";
            this.dataGridViewHires.RowTemplate.Height = 24;
            this.dataGridViewHires.Size = new System.Drawing.Size(991, 348);
            this.dataGridViewHires.TabIndex = 12;
            this.dataGridViewHires.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewReservation_CellClick);
            // 
            // EndHire
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 620);
            this.Controls.Add(this.buttonEndHire);
            this.Controls.Add(this.labelChoosenData);
            this.Controls.Add(this.dataGridViewChoosenData);
            this.Controls.Add(this.labelClientLastNAme);
            this.Controls.Add(this.labelClientName);
            this.Controls.Add(this.textBoxClientLastName);
            this.Controls.Add(this.textBoxClientName);
            this.Controls.Add(this.dataGridViewHires);
            this.Name = "EndHire";
            this.Text = "Zakończ wypożyczenie";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewChoosenData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHires)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonEndHire;
        private System.Windows.Forms.Label labelChoosenData;
        private System.Windows.Forms.DataGridView dataGridViewChoosenData;
        private System.Windows.Forms.Label labelClientLastNAme;
        private System.Windows.Forms.Label labelClientName;
        private System.Windows.Forms.TextBox textBoxClientLastName;
        private System.Windows.Forms.TextBox textBoxClientName;
        private System.Windows.Forms.DataGridView dataGridViewHires;
    }
}