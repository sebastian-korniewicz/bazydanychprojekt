﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace wypozyczalnia
{
    class AddRecords
    {
        static SqlCommand sqlCommand;
        public static void addNewItem(SqlConnection sqlConnection,string nazwaKategorii, string nazwa, string wysokoscKaucji, string cenaWypozyczenia, string LiczbaPrzedmiotow)
        {
            try
            {
                string query = $"select id_kategoria from Kategorie where nazwa='{nazwaKategorii}' ";
                decimal id_kategorii;

                sqlConnection.Open();
                using (var command = new SqlCommand(query, sqlConnection))
                {
                    id_kategorii = (decimal)command.ExecuteScalar();
                }
                

                query = $"insert into Przedmioty ( nazwa, wysokosc_kaucji, cena_wypozyczenia, liczba_przedmiotow, liczba_dostepnych_przedmiotow, id_kategoria) values ( '{nazwa}', {wysokoscKaucji},{cenaWypozyczenia},{LiczbaPrzedmiotow},{LiczbaPrzedmiotow}, {id_kategorii});";
                sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                MessageBox.Show("Dodano pomyślnie!");
            }
            catch (Exception e)
            {
                sqlConnection.Close();
                MessageBox.Show("Błąd danych!");
            }
           
        }
        public static void addNewAccount(SqlConnection sqlConnection, string login, string haslo, string email, string nrTelefonu, string imie,string nazwisko,decimal idKategorii)
        {
            try
            {
                string query = $"insert into Konta (login, haslo, id_adres, data_rejestracji, email, nr_telefonu, imie, nazwisko, rodzaj_konta) values ('{login}', '{haslo}',{idKategorii}, '{DateTime.Today.ToString("yyyy-MM-dd")}', '{email}', '{nrTelefonu}', '{imie}', '{nazwisko}', 0); ";
                sqlConnection.Open();
                sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                MessageBox.Show("Dodano pomyślnie!");
            }
            catch (Exception e)
            {
                sqlConnection.Close();
                MessageBox.Show("Błąd danych!");
            }

        }
        public static decimal addAddress(SqlConnection sqlConnection, string kodPocztowy, string numerBudynku, string numerMieszkania, string ulica, string miasto)
        {
            decimal id_adres=0;
            try
            {
                string query = $"insert into Adresy (kod_pocztowy, numer_budynku, numer_mieszkania, ulica, miasto) values ({kodPocztowy},{numerBudynku},{numerMieszkania}, '{ulica}', '{miasto}'); ";
                
                sqlConnection.Open();
                sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.ExecuteNonQuery();
                query = $"Select id_adres FROM Adresy where kod_pocztowy='{kodPocztowy}' and numer_budynku='{numerBudynku}' and numer_mieszkania='{numerMieszkania}' and ulica='{ulica}' and miasto='{miasto}' ";
               using (var command = new SqlCommand(query, sqlConnection))
               {
                    id_adres = (decimal)command.ExecuteScalar();
                }
                sqlConnection.Close();
            }
            catch (Exception e)
            {
                sqlConnection.Close();
                MessageBox.Show("Błąd danych!");
            }
           return id_adres;
        }
        public static void addNewReservation(SqlConnection sqlConnection, string id_przedmiot, string id_konto, string liczba_przedmiotow)
        {
            try
            {
                string date= DateTime.Today.ToString("yyyy-MM-dd")+" "+ DateTime.Now.ToString("HH:mm:ss");
                string query = $"INSERT INTO Rezerwacje(id_przedmiot, id_konto, data_stworzenie_rezerwacji, data_rezerwacji, liczba_przedmiotow) VALUES({id_przedmiot}, {id_konto}, '{date}', NULL, {liczba_przedmiotow}); ";
                MessageBox.Show(query);
                sqlConnection.Open();
                sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                MessageBox.Show("Dodano pomyślnie!");
            }
            catch (Exception e)
            {
                sqlConnection.Close();
                MessageBox.Show("Błąd danych!");
            }

        }
        public static void addNewHire(SqlConnection sqlConnection, string id_przedmiot, string id_konto,string id_pracownika_wypozyczajacego,string liczba_przedmiotow, string data_planowanego_zwrotu)
        {
            try
            {
                string date = DateTime.Today.ToString("yyyy-MM-dd") + " " + DateTime.Now.ToString("HH:mm:ss");
                string query = $"INSERT INTO Wypozyczenia ( id_przedmiot, id_konto, id_pracownika_wypozyczajacego, liczba_przedmiotow,data_wypozyczenia, data_planowanego_zwrotu) VALUES('{id_przedmiot}','{id_konto}','{id_pracownika_wypozyczajacego}','{liczba_przedmiotow}', '{date}', '{data_planowanego_zwrotu}'); ";
                sqlConnection.Open();
               // MessageBox.Show(query);
                sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                MessageBox.Show("Dodano pomyślnie!");
            }
            catch (Exception e)
            {
                sqlConnection.Close();
                MessageBox.Show("Błąd danych!");
            }

        }
        public static void addArchiveHire(SqlConnection sqlConnection,string id_wypozyczenie, string nazwa_przedmiotu, string id_pracownika_odbierajacego,string id_konto,string data_wypozyczenia, string data_planowanego_zwrotu)
        {
            decimal idPrzedmiot;
            decimal idPracownikaWypozyczajacego;
            try
            {
                sqlConnection.Open();
                string query = $"Select id_przedmiot FROM Przedmioty where nazwa='{nazwa_przedmiotu}'";
                using (var command = new SqlCommand(query, sqlConnection))
                {
                    idPrzedmiot = (decimal)command.ExecuteScalar();
                }
                query = $"Select id_pracownika_wypozyczajacego FROM Wypozyczenia where id_wypozyczenie='{id_wypozyczenie}'";
                using (var command = new SqlCommand(query, sqlConnection))
                {
                    idPracownikaWypozyczajacego = (decimal)command.ExecuteScalar();
                }
                string date = DateTime.Today.ToString("yyyy-MM-dd") + " " + DateTime.Now.ToString("HH:mm:ss");
                 query = $"insert into Archiwalne_wypozyczenia ( id_konto, id_przedmiot, id_pracownika_wypozyczajacego, id_pracownika_odbierajacego,  data_wypozyczenia, data_planowanego_zwrotu, data_zwrotu) values ( '{id_konto}','{idPrzedmiot.ToString()}','{idPracownikaWypozyczajacego.ToString()}', '{id_pracownika_odbierajacego}', '{data_wypozyczenia}', '{data_planowanego_zwrotu}', '{date}'); ";
               
                 MessageBox.Show(query);
                sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                MessageBox.Show("Dodano pomyślnie!");
                RemoveRecords.RemoveHire(sqlConnection, id_wypozyczenie);
            }
            catch (Exception e)
            {
                sqlConnection.Close();
                MessageBox.Show("Błąd danych!");
            }

        }
    }
}
