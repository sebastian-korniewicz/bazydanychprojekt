﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace wypozyczalnia
{
    class RemoveRecords
    {
        static SqlCommand sqlCommand;
        public static void RemoveReservation(SqlConnection sqlConnection,  string id_przedmiotu,string id_konta, string data_rezerwacji,string liczba_przedmiotow)
        {
            try
            {
                //and data_rezerwacji='{data_rezerwacji}'
                sqlConnection.Open();
                string command = $"DELETE FROM Rezerwacje WHERE id_przedmiot='{id_przedmiotu}' and id_konto='{id_konta}'  and liczba_przedmiotow='{liczba_przedmiotow}' ;";
                
                sqlCommand = new SqlCommand(command, sqlConnection);
                sqlCommand.ExecuteNonQuery();
                MessageBox.Show("Usunięto");
                sqlConnection.Close();
            }
            catch (Exception e)
            {
                sqlConnection.Close();
                MessageBox.Show("Błąd danych!");
            }
        }
        public static void RemoveHire(SqlConnection sqlConnection, string id_wypozyczenie)
        {
            try
            {
                sqlConnection.Open();
                string command = $"DELETE FROM Wypozyczenia WHERE  id_wypozyczenie='{id_wypozyczenie}' ;";
                sqlCommand = new SqlCommand(command, sqlConnection);
                sqlCommand.ExecuteNonQuery();
                MessageBox.Show("Usunięto");
                sqlConnection.Close();
            }
            catch (Exception e)
            {
                sqlConnection.Close();
                MessageBox.Show("Błąd danych!");
            }
        }
    }
}
