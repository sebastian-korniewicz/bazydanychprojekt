﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace wypozyczalnia
{
    public partial class MainApp : Form
    {
        SqlConnection sqlConnection;
        public static int AccountId { get; set; }
        public MainApp(string accountType,int accountId, String configurationString)
        {
            AccountId = accountId;

            StringBuilder sb = new StringBuilder();
            sb.Append("Data Source=");
            sb.Append(configurationString);
            sb.Append(";Initial Catalog=wypozyczalnia;User ID=");
            sb.Append(accountType);
            sb.Append("; Password = 'a'");

            sqlConnection = new SqlConnection(sb.ToString());
            InitializeComponent();
        }
        private void btnAddItem_Click(object sender, EventArgs e)
        {
            AddNewItem addNewItem = new AddNewItem(sqlConnection);
            addNewItem.Visible = true;
        }

        private void btnAddAccount_Click(object sender, EventArgs e)
        {
            AddAccount addNewAccount = new AddAccount(sqlConnection);
            addNewAccount.Visible = true;
        }

        private void btnAddReservation_Click(object sender, EventArgs e)
        {
            AddReservation addNewReservation = new AddReservation(sqlConnection);
            addNewReservation.Visible = true;
        }

        private void btnEditItem_Click(object sender, EventArgs e)
        {
            EditItem editItem = new EditItem(sqlConnection);
            editItem.Visible = true;
        }

        private void btnEditAccount_Click(object sender, EventArgs e)
        {
            EditAccount editAccount = new EditAccount(sqlConnection);
            editAccount.Visible = true;
        }

        private void btnCancelReservation_Click(object sender, EventArgs e)
        {
            CancelReservation cancelReservation = new CancelReservation(sqlConnection);
            cancelReservation.Visible = true;
        }

        private void btnHire_Click(object sender, EventArgs e)
        {
            AddNewHire addNewHire = new AddNewHire(sqlConnection);
            addNewHire.Visible = true;
        }

        private void btnReturnItem_Click(object sender, EventArgs e)
        {
            EndHire endHire = new EndHire(sqlConnection);
            endHire.Visible = true;
        }

        private void btnListOfItems_Click(object sender, EventArgs e)
        {
            btnListOfItems.Enabled = false;
            buttonListOfArchive.Enabled = true;
            btnListOfHires.Enabled = true;
            btnListOfReservation.Enabled = true;
            ShowLists();
        }

        private void buttonListOfReservation_Click(object sender, EventArgs e)
        {
            btnListOfItems.Enabled = true;
            btnListOfReservation.Enabled = false;
            btnListOfHires.Enabled = true;
            buttonListOfArchive.Enabled = true;
            ShowLists();
        }

        private void btnListOfHires_Click(object sender, EventArgs e)
        {
            btnListOfItems.Enabled = true;
            btnListOfReservation.Enabled =true;
            btnListOfHires.Enabled = false;
            buttonListOfArchive.Enabled = true;
            ShowLists();
        }

        private void buttonListOfArchive_Click(object sender, EventArgs e)
        {
            btnListOfItems.Enabled = true;
            btnListOfHires.Enabled =true;
            btnListOfReservation.Enabled =true;

            buttonListOfArchive.Enabled = false;
            ShowLists();

        }


        private void ShowLists()
        {
            if(btnListOfReservation.Enabled==false)
            {
                ShowRecords.ShowReservation(sqlConnection, dataGridViewMain, textBoxFiltr.Text);
            }
            else if(btnListOfHires.Enabled==false)
            {
                ShowRecords.ShowHires(sqlConnection, dataGridViewMain, textBoxFiltr.Text);
            }
            else if(buttonListOfArchive.Enabled==false)
            {
                ShowRecords.ShowArchiveHires(sqlConnection, dataGridViewMain, textBoxFiltr.Text);
            }
            else if (btnListOfItems.Enabled == false)
            {
                ShowRecords.ShowItems(sqlConnection, dataGridViewMain, textBoxFiltr.Text, null);
            }
            else
            {
                MessageBox.Show("Musisz wybrac gdzie chcesz szukac!");
            }
        }

        private void textBoxFiltr_TextChanged(object sender, EventArgs e)
        {
            ShowLists();
        }

        private void MainApp_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }


    }
}
