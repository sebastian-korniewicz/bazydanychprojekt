﻿namespace wypozyczalnia
{
    partial class CancelReservation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewReservation = new System.Windows.Forms.DataGridView();
            this.textBoxClientName = new System.Windows.Forms.TextBox();
            this.textBoxClientLastName = new System.Windows.Forms.TextBox();
            this.labelClientName = new System.Windows.Forms.Label();
            this.labelClientLastNAme = new System.Windows.Forms.Label();
            this.dataGridViewChoosenData = new System.Windows.Forms.DataGridView();
            this.labelChoosenData = new System.Windows.Forms.Label();
            this.buttonCancelReservation = new System.Windows.Forms.Button();
            this.labelDateTimePicker = new System.Windows.Forms.Label();
            this.labelItemName = new System.Windows.Forms.Label();
            this.textBoxItemName = new System.Windows.Forms.TextBox();
            this.dateTimePickerReservation = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReservation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewChoosenData)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewReservation
            // 
            this.dataGridViewReservation.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewReservation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewReservation.Location = new System.Drawing.Point(9, 83);
            this.dataGridViewReservation.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridViewReservation.Name = "dataGridViewReservation";
            this.dataGridViewReservation.RowTemplate.Height = 24;
            this.dataGridViewReservation.Size = new System.Drawing.Size(743, 283);
            this.dataGridViewReservation.TabIndex = 0;
            this.dataGridViewReservation.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewReservation_CellClick);
            // 
            // textBoxClientName
            // 
            this.textBoxClientName.Location = new System.Drawing.Point(119, 30);
            this.textBoxClientName.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxClientName.Name = "textBoxClientName";
            this.textBoxClientName.Size = new System.Drawing.Size(151, 20);
            this.textBoxClientName.TabIndex = 2;
            this.textBoxClientName.TextChanged += new System.EventHandler(this.textBoxClientName_TextChanged);
            // 
            // textBoxClientLastName
            // 
            this.textBoxClientLastName.Location = new System.Drawing.Point(119, 53);
            this.textBoxClientLastName.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxClientLastName.Name = "textBoxClientLastName";
            this.textBoxClientLastName.Size = new System.Drawing.Size(151, 20);
            this.textBoxClientLastName.TabIndex = 3;
            this.textBoxClientLastName.TextChanged += new System.EventHandler(this.textBoxClientLastName_TextChanged);
            // 
            // labelClientName
            // 
            this.labelClientName.AutoSize = true;
            this.labelClientName.Location = new System.Drawing.Point(54, 32);
            this.labelClientName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelClientName.Name = "labelClientName";
            this.labelClientName.Size = new System.Drawing.Size(26, 13);
            this.labelClientName.TabIndex = 5;
            this.labelClientName.Text = "Imie";
            // 
            // labelClientLastNAme
            // 
            this.labelClientLastNAme.AutoSize = true;
            this.labelClientLastNAme.Location = new System.Drawing.Point(54, 53);
            this.labelClientLastNAme.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelClientLastNAme.Name = "labelClientLastNAme";
            this.labelClientLastNAme.Size = new System.Drawing.Size(53, 13);
            this.labelClientLastNAme.TabIndex = 6;
            this.labelClientLastNAme.Text = "Nazwisko";
            // 
            // dataGridViewChoosenData
            // 
            this.dataGridViewChoosenData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewChoosenData.Location = new System.Drawing.Point(9, 399);
            this.dataGridViewChoosenData.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridViewChoosenData.Name = "dataGridViewChoosenData";
            this.dataGridViewChoosenData.RowTemplate.Height = 24;
            this.dataGridViewChoosenData.Size = new System.Drawing.Size(743, 46);
            this.dataGridViewChoosenData.TabIndex = 9;
            // 
            // labelChoosenData
            // 
            this.labelChoosenData.AutoSize = true;
            this.labelChoosenData.Location = new System.Drawing.Point(9, 375);
            this.labelChoosenData.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelChoosenData.Name = "labelChoosenData";
            this.labelChoosenData.Size = new System.Drawing.Size(53, 13);
            this.labelChoosenData.TabIndex = 10;
            this.labelChoosenData.Text = "Wybrano:";
            // 
            // buttonCancelReservation
            // 
            this.buttonCancelReservation.Location = new System.Drawing.Point(653, 454);
            this.buttonCancelReservation.Margin = new System.Windows.Forms.Padding(2);
            this.buttonCancelReservation.Name = "buttonCancelReservation";
            this.buttonCancelReservation.Size = new System.Drawing.Size(99, 26);
            this.buttonCancelReservation.TabIndex = 11;
            this.buttonCancelReservation.Text = "Usun rezerwacje";
            this.buttonCancelReservation.UseVisualStyleBackColor = true;
            this.buttonCancelReservation.Click += new System.EventHandler(this.buttonCancelReservation_Click);
            // 
            // labelDateTimePicker
            // 
            this.labelDateTimePicker.AutoSize = true;
            this.labelDateTimePicker.Location = new System.Drawing.Point(410, 51);
            this.labelDateTimePicker.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelDateTimePicker.Name = "labelDateTimePicker";
            this.labelDateTimePicker.Size = new System.Drawing.Size(30, 13);
            this.labelDateTimePicker.TabIndex = 24;
            this.labelDateTimePicker.Text = "Data";
            // 
            // labelItemName
            // 
            this.labelItemName.AutoSize = true;
            this.labelItemName.Location = new System.Drawing.Point(410, 28);
            this.labelItemName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelItemName.Name = "labelItemName";
            this.labelItemName.Size = new System.Drawing.Size(94, 13);
            this.labelItemName.TabIndex = 23;
            this.labelItemName.Text = "Nazwa przedmiotu";
            // 
            // textBoxItemName
            // 
            this.textBoxItemName.Location = new System.Drawing.Point(508, 28);
            this.textBoxItemName.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxItemName.Name = "textBoxItemName";
            this.textBoxItemName.Size = new System.Drawing.Size(151, 20);
            this.textBoxItemName.TabIndex = 22;
            // 
            // dateTimePickerReservation
            // 
            this.dateTimePickerReservation.Location = new System.Drawing.Point(508, 51);
            this.dateTimePickerReservation.Margin = new System.Windows.Forms.Padding(2);
            this.dateTimePickerReservation.Name = "dateTimePickerReservation";
            this.dateTimePickerReservation.Size = new System.Drawing.Size(151, 20);
            this.dateTimePickerReservation.TabIndex = 21;
            this.dateTimePickerReservation.ValueChanged += new System.EventHandler(this.dateTimePickerReservation_ValueChanged_1);
            // 
            // CancelReservation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(761, 490);
            this.Controls.Add(this.labelDateTimePicker);
            this.Controls.Add(this.labelItemName);
            this.Controls.Add(this.textBoxItemName);
            this.Controls.Add(this.dateTimePickerReservation);
            this.Controls.Add(this.buttonCancelReservation);
            this.Controls.Add(this.labelChoosenData);
            this.Controls.Add(this.dataGridViewChoosenData);
            this.Controls.Add(this.labelClientLastNAme);
            this.Controls.Add(this.labelClientName);
            this.Controls.Add(this.textBoxClientLastName);
            this.Controls.Add(this.textBoxClientName);
            this.Controls.Add(this.dataGridViewReservation);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "CancelReservation";
            this.Text = "Anuluj rezerwację";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReservation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewChoosenData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewReservation;
        private System.Windows.Forms.TextBox textBoxClientName;
        private System.Windows.Forms.TextBox textBoxClientLastName;
        private System.Windows.Forms.Label labelClientName;
        private System.Windows.Forms.Label labelClientLastNAme;
        private System.Windows.Forms.DataGridView dataGridViewChoosenData;
        private System.Windows.Forms.Label labelChoosenData;
        private System.Windows.Forms.Button buttonCancelReservation;
        private System.Windows.Forms.Label labelDateTimePicker;
        private System.Windows.Forms.Label labelItemName;
        private System.Windows.Forms.TextBox textBoxItemName;
        private System.Windows.Forms.DateTimePicker dateTimePickerReservation;
    }
}