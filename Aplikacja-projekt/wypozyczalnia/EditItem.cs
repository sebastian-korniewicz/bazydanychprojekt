﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace wypozyczalnia
{
    public partial class EditItem : Form
    {
        SqlConnection sqlConnection;
        public EditItem(SqlConnection sqlConnection)
        {
            this.sqlConnection = sqlConnection;
            InitializeComponent();
            
        }

        private void textBoxItemName_TextChanged(object sender, EventArgs e)
        {
            ShowRecords.ShowItems(sqlConnection, dataGridViewItem, textBoxItemName.Text, textBoxItemCategory.Text);
            dataGridViewItem.Columns[0].ReadOnly = true;
            dataGridViewItem.Columns[1].ReadOnly = true;
            dataGridViewItem.Columns[6].ReadOnly = true;
        }

        private void textBoxItemCategory_TextChanged(object sender, EventArgs e)
        {
            ShowRecords.ShowItems(sqlConnection, dataGridViewItem, textBoxItemName.Text, textBoxItemCategory.Text);
            dataGridViewItem.Columns[0].ReadOnly = true;
            dataGridViewItem.Columns[1].ReadOnly = true;
            dataGridViewItem.Columns[6].ReadOnly = true;
        }

        private void dataGridViewItem_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            EditRecords.editItem(sqlConnection, dataGridViewItem.Rows[e.RowIndex].Cells[0].Value.ToString(), dataGridViewItem.Rows[e.RowIndex].Cells[2].Value.ToString(),dataGridViewItem.Rows[e.RowIndex].Cells[3].Value.ToString(),dataGridViewItem.Rows[e.RowIndex].Cells[4].Value.ToString(),dataGridViewItem.Rows[e.RowIndex].Cells[5].Value.ToString());
            ShowRecords.ShowItems(sqlConnection, dataGridViewItem, textBoxItemName.Text, textBoxItemCategory.Text);
        }
    }
}
