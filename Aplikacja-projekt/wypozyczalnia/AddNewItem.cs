﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace wypozyczalnia
{
    public partial class AddNewItem : Form
    {
        SqlConnection sqlConnection;
        public AddNewItem(SqlConnection sqlConnection)
        {
            this.sqlConnection = sqlConnection;
            InitializeComponent();
            comboBoxCategoryColumns.Items.Add("");
            var Columns = ShowRecords.ListOfCategoryColumns(sqlConnection);
            foreach (string n in Columns)
            {
                comboBoxCategoryColumns.Items.Add(n);
            }
            comboBoxCategoryColumns.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void btnAddItemForm_Click(object sender, EventArgs e)
        {
            AddRecords.addNewItem(sqlConnection, comboBoxCategoryColumns.SelectedItem.ToString(), textBoxItemName.Text, textBoxDeposit.Text, textBoxHirePrice.Text, textBoxNumberOfItems.Text);
        }
    }
}
