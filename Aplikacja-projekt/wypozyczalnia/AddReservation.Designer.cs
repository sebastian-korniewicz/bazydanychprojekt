﻿namespace wypozyczalnia
{
    partial class AddReservation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxItemName = new System.Windows.Forms.TextBox();
            this.textBoxCategoryName = new System.Windows.Forms.TextBox();
            this.labelItemName = new System.Windows.Forms.Label();
            this.labelCategoryName = new System.Windows.Forms.Label();
            this.labelClientLastName = new System.Windows.Forms.Label();
            this.labelClientName = new System.Windows.Forms.Label();
            this.textBoxClientLastName = new System.Windows.Forms.TextBox();
            this.textBoxClientName = new System.Windows.Forms.TextBox();
            this.labelChosenItem = new System.Windows.Forms.Label();
            this.labelChooseClient = new System.Windows.Forms.Label();
            this.dataGridViewItem = new System.Windows.Forms.DataGridView();
            this.dataGridViewAccount = new System.Windows.Forms.DataGridView();
            this.textBoxItemQtty = new System.Windows.Forms.TextBox();
            this.labelItemQtty = new System.Windows.Forms.Label();
            this.buttonAddReservation = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAccount)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxItemName
            // 
            this.textBoxItemName.Location = new System.Drawing.Point(156, 29);
            this.textBoxItemName.Name = "textBoxItemName";
            this.textBoxItemName.Size = new System.Drawing.Size(141, 22);
            this.textBoxItemName.TabIndex = 2;
            this.textBoxItemName.TextChanged += new System.EventHandler(this.textBoxItemName_TextChanged);
            // 
            // textBoxCategoryName
            // 
            this.textBoxCategoryName.Location = new System.Drawing.Point(156, 57);
            this.textBoxCategoryName.Name = "textBoxCategoryName";
            this.textBoxCategoryName.Size = new System.Drawing.Size(141, 22);
            this.textBoxCategoryName.TabIndex = 3;
            this.textBoxCategoryName.TextChanged += new System.EventHandler(this.textBoxCategoryName_TextChanged);
            // 
            // labelItemName
            // 
            this.labelItemName.AutoSize = true;
            this.labelItemName.Location = new System.Drawing.Point(12, 29);
            this.labelItemName.Name = "labelItemName";
            this.labelItemName.Size = new System.Drawing.Size(124, 17);
            this.labelItemName.TabIndex = 4;
            this.labelItemName.Text = "Nazwa przedmiotu";
            // 
            // labelCategoryName
            // 
            this.labelCategoryName.AutoSize = true;
            this.labelCategoryName.Location = new System.Drawing.Point(12, 57);
            this.labelCategoryName.Name = "labelCategoryName";
            this.labelCategoryName.Size = new System.Drawing.Size(108, 17);
            this.labelCategoryName.TabIndex = 5;
            this.labelCategoryName.Text = "Nazwa kategorii";
            // 
            // labelClientLastName
            // 
            this.labelClientLastName.AutoSize = true;
            this.labelClientLastName.Location = new System.Drawing.Point(12, 348);
            this.labelClientLastName.Name = "labelClientLastName";
            this.labelClientLastName.Size = new System.Drawing.Size(112, 17);
            this.labelClientLastName.TabIndex = 9;
            this.labelClientLastName.Text = "Nazwisko klienta";
            // 
            // labelClientName
            // 
            this.labelClientName.AutoSize = true;
            this.labelClientName.Location = new System.Drawing.Point(12, 320);
            this.labelClientName.Name = "labelClientName";
            this.labelClientName.Size = new System.Drawing.Size(78, 17);
            this.labelClientName.TabIndex = 8;
            this.labelClientName.Text = "Imie klienta";
            // 
            // textBoxClientLastName
            // 
            this.textBoxClientLastName.Location = new System.Drawing.Point(156, 348);
            this.textBoxClientLastName.Name = "textBoxClientLastName";
            this.textBoxClientLastName.Size = new System.Drawing.Size(141, 22);
            this.textBoxClientLastName.TabIndex = 7;
            this.textBoxClientLastName.TextChanged += new System.EventHandler(this.textBoxClientLastName_TextChanged);
            // 
            // textBoxClientName
            // 
            this.textBoxClientName.Location = new System.Drawing.Point(156, 320);
            this.textBoxClientName.Name = "textBoxClientName";
            this.textBoxClientName.Size = new System.Drawing.Size(141, 22);
            this.textBoxClientName.TabIndex = 6;
            this.textBoxClientName.TextChanged += new System.EventHandler(this.textBoxClientName_TextChanged);
            // 
            // labelChosenItem
            // 
            this.labelChosenItem.AutoSize = true;
            this.labelChosenItem.Location = new System.Drawing.Point(408, 60);
            this.labelChosenItem.Name = "labelChosenItem";
            this.labelChosenItem.Size = new System.Drawing.Size(69, 17);
            this.labelChosenItem.TabIndex = 10;
            this.labelChosenItem.Text = "Wybrano:";
            // 
            // labelChooseClient
            // 
            this.labelChooseClient.AutoSize = true;
            this.labelChooseClient.Location = new System.Drawing.Point(408, 351);
            this.labelChooseClient.Name = "labelChooseClient";
            this.labelChooseClient.Size = new System.Drawing.Size(69, 17);
            this.labelChooseClient.TabIndex = 11;
            this.labelChooseClient.Text = "Wybrano:";
            // 
            // dataGridViewItem
            // 
            this.dataGridViewItem.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewItem.Location = new System.Drawing.Point(15, 85);
            this.dataGridViewItem.Name = "dataGridViewItem";
            this.dataGridViewItem.RowTemplate.Height = 24;
            this.dataGridViewItem.Size = new System.Drawing.Size(1017, 186);
            this.dataGridViewItem.TabIndex = 12;
            this.dataGridViewItem.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewItem_CellClick);
            // 
            // dataGridViewAccount
            // 
            this.dataGridViewAccount.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewAccount.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAccount.Location = new System.Drawing.Point(15, 380);
            this.dataGridViewAccount.Name = "dataGridViewAccount";
            this.dataGridViewAccount.RowTemplate.Height = 24;
            this.dataGridViewAccount.Size = new System.Drawing.Size(1017, 167);
            this.dataGridViewAccount.TabIndex = 13;
            this.dataGridViewAccount.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewAccount_CellClick);
            // 
            // textBoxItemQtty
            // 
            this.textBoxItemQtty.Location = new System.Drawing.Point(903, 57);
            this.textBoxItemQtty.Name = "textBoxItemQtty";
            this.textBoxItemQtty.Size = new System.Drawing.Size(129, 22);
            this.textBoxItemQtty.TabIndex = 14;
            // 
            // labelItemQtty
            // 
            this.labelItemQtty.AutoSize = true;
            this.labelItemQtty.Location = new System.Drawing.Point(790, 62);
            this.labelItemQtty.Name = "labelItemQtty";
            this.labelItemQtty.Size = new System.Drawing.Size(40, 17);
            this.labelItemQtty.TabIndex = 15;
            this.labelItemQtty.Text = "Ilość:";
            // 
            // buttonAddReservation
            // 
            this.buttonAddReservation.Location = new System.Drawing.Point(914, 574);
            this.buttonAddReservation.Name = "buttonAddReservation";
            this.buttonAddReservation.Size = new System.Drawing.Size(118, 57);
            this.buttonAddReservation.TabIndex = 16;
            this.buttonAddReservation.Text = "Dodaj rezerwacje";
            this.buttonAddReservation.UseVisualStyleBackColor = true;
            this.buttonAddReservation.Click += new System.EventHandler(this.buttonAddReservation_Click);
            // 
            // AddReservation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1056, 643);
            this.Controls.Add(this.buttonAddReservation);
            this.Controls.Add(this.labelItemQtty);
            this.Controls.Add(this.textBoxItemQtty);
            this.Controls.Add(this.dataGridViewAccount);
            this.Controls.Add(this.dataGridViewItem);
            this.Controls.Add(this.labelChooseClient);
            this.Controls.Add(this.labelChosenItem);
            this.Controls.Add(this.labelClientLastName);
            this.Controls.Add(this.labelClientName);
            this.Controls.Add(this.textBoxClientLastName);
            this.Controls.Add(this.textBoxClientName);
            this.Controls.Add(this.labelCategoryName);
            this.Controls.Add(this.labelItemName);
            this.Controls.Add(this.textBoxCategoryName);
            this.Controls.Add(this.textBoxItemName);
            this.Name = "AddReservation";
            this.Text = "Dodaj rezerwację";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAccount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox textBoxItemName;
        private System.Windows.Forms.TextBox textBoxCategoryName;
        private System.Windows.Forms.Label labelItemName;
        private System.Windows.Forms.Label labelCategoryName;
        private System.Windows.Forms.Label labelClientLastName;
        private System.Windows.Forms.Label labelClientName;
        private System.Windows.Forms.TextBox textBoxClientLastName;
        private System.Windows.Forms.TextBox textBoxClientName;
        private System.Windows.Forms.Label labelChosenItem;
        private System.Windows.Forms.Label labelChooseClient;
        private System.Windows.Forms.DataGridView dataGridViewItem;
        private System.Windows.Forms.DataGridView dataGridViewAccount;
        private System.Windows.Forms.TextBox textBoxItemQtty;
        private System.Windows.Forms.Label labelItemQtty;
        private System.Windows.Forms.Button buttonAddReservation;
    }
}