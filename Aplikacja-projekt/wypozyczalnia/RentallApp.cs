﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace wypozyczalnia
{
    public partial class RentallApp : Form
    {
        SqlConnection sqlConnection;

        public static string ConfigurationString;

        public RentallApp()
        {
            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader("Konfiguracja.txt"))
                {
                    ConfigurationString = sr.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            StringBuilder sb = new StringBuilder();
            //sb.Append("Data Source=");
            //sb.Append(ConfigurationString);
            //sb.Append("; database=wypozyczalnia;Trusted_Connection=no");

            sb.Append("Data Source=");
            sb.Append(ConfigurationString);
            sb.Append(";Initial Catalog=wypozyczalnia;User ID=");
            sb.Append("klient");
            sb.Append("; Password = klient");


            InitializeComponent();

            sqlConnection = new SqlConnection(sb.ToString());
            try
            {
                sqlConnection.Open();
                sqlConnection.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("Brak polaczenia z serwerem");
            }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            int log = LogIn.checkLog(sqlConnection, textBoxLogin.Text, textBoxPassword.Text);
            if (log==4) MessageBox.Show("Zle dane!");
            else
            {
                string user = null; 
                if (log == 0) user = "klient";
                if (log == 1) user = "pracownik";
                if (log == 2) user = "admin";
                if(user=="pracownik")
                {
                    sqlConnection.Close();
                    MainApp mainApp = new MainApp(user,LogIn.getAccountId(sqlConnection, textBoxLogin.Text, textBoxPassword.Text), ConfigurationString);
                    mainApp.Visible = true;
                    this.Visible = false;
                }
                else if(user!=null && user!="pracownik")
                {
                    MessageBox.Show("Tylko pracownik może się zalogować!");
                }
                else
                {
                    MessageBox.Show("Cos poszlo nie tak, sprobuj jeszcze raz!");
                }
            }
        }
    }
}
