﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace wypozyczalnia
{
    public partial class EndHire : Form
    {
        SqlConnection sqlConnection;
        public EndHire(SqlConnection sqlConnection)
        {
            this.sqlConnection = sqlConnection;
            InitializeComponent();
            
        }
        private void textBoxClientName_TextChanged(object sender, EventArgs e)
        {
            ShowRecords.ShowHires(sqlConnection, dataGridViewHires, textBoxClientName.Text, textBoxClientLastName.Text);
        }

        private void textBoxClientLastName_TextChanged(object sender, EventArgs e)
        {
            ShowRecords.ShowHires(sqlConnection, dataGridViewHires, textBoxClientName.Text, textBoxClientLastName.Text);
        }
        private void dataGridViewReservation_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            dataGridViewChoosenData.Rows.Clear();
            DataGridViewColumn newCol = null;
            if (dataGridViewChoosenData.Columns.Count == 0)
            {
                foreach (DataGridViewColumn col in dataGridViewHires.Columns)
                {
                    newCol = new DataGridViewColumn(col.CellTemplate);
                    newCol.HeaderText = col.HeaderText;
                    newCol.Name = col.Name;
                    dataGridViewChoosenData.Columns.Add(newCol);
                }
            }
            foreach (DataGridViewColumn col in dataGridViewHires.Columns)
            {
                dataGridViewChoosenData.Rows[0].Cells[col.Name].Value = dataGridViewHires.Rows[dataGridViewHires.CurrentCell.RowIndex].Cells[col.Name].Value;
            }

        }

        private void buttonEndHire_Click(object sender, EventArgs e)
        {
            AddRecords.addArchiveHire(sqlConnection,
                dataGridViewChoosenData.Rows[0].Cells[0].Value.ToString(),
                dataGridViewChoosenData.Rows[0].Cells[6].Value.ToString(),
                MainApp.AccountId.ToString(),
                dataGridViewChoosenData.Rows[0].Cells[3].Value.ToString(),
                dataGridViewChoosenData.Rows[0].Cells[1].Value.ToString(),
                dataGridViewChoosenData.Rows[0].Cells[2].Value.ToString());
            dataGridViewChoosenData.Rows.Clear();
            ShowRecords.ShowHires(sqlConnection, dataGridViewHires, textBoxClientName.Text, textBoxClientLastName.Text);
        }
    }
}
