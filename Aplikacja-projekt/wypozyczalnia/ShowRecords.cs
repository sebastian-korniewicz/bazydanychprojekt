﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace wypozyczalnia
{
    class ShowRecords
    {
        public static List<string> ListOfCategoryColumns(SqlConnection sqlConnection)
        {
            sqlConnection.Open();
            var columns = new List<string>();
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter("SELECT nazwa FROM Kategorie", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            columns = dataTable.AsEnumerable().Select(n => n.Field<string>(0)).ToList();
            sqlConnection.Close();
            return columns;//pobieram wszystkie tablice z mojej bazy danych
        }
        public static void ShowItems(SqlConnection sqlConnection, DataGridView dataGridView, string itemName, string categoryName)
        {
           
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter($@"SELECT * from Przedmiot where nazwa Like '%{itemName}%' and ""nazwa kategorii"" like '%{categoryName}%'", sqlConnection);//wysylam zapytanie
            FillDataGridView(sqlDataAdapter, dataGridView);
        }
        private static void FillDataGridView(SqlDataAdapter sqlDataAdapter, DataGridView dataGridView)
        {
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }
        public static void ShowClient(SqlConnection sqlConnection, DataGridView dataGridView, string clientName, string clientLastName)
        {

            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter($@"SELECT * from Konto where imie Like '%{clientName}%' and nazwisko like '%{clientLastName}%'", sqlConnection);//wysylam zapytanie
            FillDataGridView(sqlDataAdapter, dataGridView);
        }
        public static void ShowReservation(SqlConnection sqlConnection, DataGridView dataGridView, string clientName, string clientLastName, string itemName)
        {
            //MessageBox.Show($@"SELECT * from Rezerwacja where imie Like '%{clientName}%' and nazwisko like '%{clientLastName}%' and nazwa like'%{itemName}%' and ""Data rezerwacji"" = '{date}'");
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter($@"SELECT * from Rezerwacja where imie Like '%{clientName}%' and nazwisko like '%{clientLastName}%' and nazwa like'%{itemName}%'", sqlConnection);//wysylam zapytanie
            FillDataGridView(sqlDataAdapter, dataGridView);
        }
        public static void ShowReservation(SqlConnection sqlConnection, DataGridView dataGridView, string clientName, string clientLastName,string itemName,string date)
        {
            //MessageBox.Show($@"SELECT * from Rezerwacja where imie Like '%{clientName}%' and nazwisko like '%{clientLastName}%' and nazwa like'%{itemName}%' and ""Data rezerwacji"" = '{date}'");
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter($@"SELECT * from Rezerwacja where imie Like '%{clientName}%' and nazwisko like '%{clientLastName}%' and nazwa like'%{itemName}%' and ""Data rezerwacji"" = '{date}'", sqlConnection);//wysylam zapytanie
            FillDataGridView(sqlDataAdapter, dataGridView);
        }
        public static void ShowReservation(SqlConnection sqlConnection, DataGridView dataGridView, string filtr)
        {
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter($@"SELECT * from Rezerwacja where imie Like '%{filtr}%' or nazwisko like '%{filtr}%' or nazwa like'%{filtr}%' or ""Data rezerwacji"" like '%{filtr}%'", sqlConnection);//wysylam zapytanie
            FillDataGridView(sqlDataAdapter, dataGridView);
        }
        public static void ShowHires(SqlConnection sqlConnection, DataGridView dataGridView, string clientName, string clientLastName)
        {
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter($@"SELECT * from Wypozyczenie where imie Like '%{clientName}%' and nazwisko like '%{clientLastName}%'", sqlConnection);//wysylam zapytanie
            FillDataGridView(sqlDataAdapter, dataGridView);
        }
        public static void ShowHires(SqlConnection sqlConnection, DataGridView dataGridView, string filtr)
        {
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter($@"SELECT * from Wypozyczenie where imie Like '%{filtr}%' or nazwisko like '%{filtr}%' or ""nazwa przedmiotu"" like '%{filtr}%' or data_wypozyczenia like'%{filtr}%'", sqlConnection);//wysylam zapytanie
            FillDataGridView(sqlDataAdapter, dataGridView);
        }
        public static void ShowArchiveHires(SqlConnection sqlConnection, DataGridView dataGridView, string filtr)
        {
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter($@"SELECT * from ArchiwalneWypozyczenie where imie Like '%{filtr}%' or nazwisko like '%{filtr}%' or nazwa like '%{filtr}%' or data_wypozyczenia like '%{filtr}%' or nr_telefonu like '%{filtr}%' or data_wypozyczenia like '%{filtr}%' or data_planowanego_zwrotu like '%{filtr}%' or data_zwrotu like '%{filtr}%' or id_pracownika_wypozyczajacego like '%{filtr}%' or
      id_pracownika_odbierajacego like '%{filtr}%' ", sqlConnection);//wysylam zapytanie
            FillDataGridView(sqlDataAdapter, dataGridView);
        }
    }
}
