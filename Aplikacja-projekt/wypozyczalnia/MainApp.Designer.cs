﻿namespace wypozyczalnia
{
    partial class MainApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAddItem = new System.Windows.Forms.Button();
            this.btnReturnItem = new System.Windows.Forms.Button();
            this.btnCancelReservation = new System.Windows.Forms.Button();
            this.btnEditItem = new System.Windows.Forms.Button();
            this.btnAddAccount = new System.Windows.Forms.Button();
            this.btnAddReservation = new System.Windows.Forms.Button();
            this.btnHire = new System.Windows.Forms.Button();
            this.btnListOfHires = new System.Windows.Forms.Button();
            this.btnEditAccount = new System.Windows.Forms.Button();
            this.textBoxFiltr = new System.Windows.Forms.TextBox();
            this.labelFiltr = new System.Windows.Forms.Label();
            this.dataGridViewMain = new System.Windows.Forms.DataGridView();
            this.btnListOfReservation = new System.Windows.Forms.Button();
            this.buttonListOfArchive = new System.Windows.Forms.Button();
            this.btnListOfItems = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMain)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAddItem
            // 
            this.btnAddItem.Location = new System.Drawing.Point(12, 12);
            this.btnAddItem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAddItem.Name = "btnAddItem";
            this.btnAddItem.Size = new System.Drawing.Size(140, 30);
            this.btnAddItem.TabIndex = 0;
            this.btnAddItem.Text = "Dodaj przedmiot";
            this.btnAddItem.UseVisualStyleBackColor = true;
            this.btnAddItem.Click += new System.EventHandler(this.btnAddItem_Click);
            // 
            // btnReturnItem
            // 
            this.btnReturnItem.Location = new System.Drawing.Point(157, 84);
            this.btnReturnItem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnReturnItem.Name = "btnReturnItem";
            this.btnReturnItem.Size = new System.Drawing.Size(140, 30);
            this.btnReturnItem.TabIndex = 5;
            this.btnReturnItem.Text = "Zwrot przedmiotu";
            this.btnReturnItem.UseVisualStyleBackColor = true;
            this.btnReturnItem.Click += new System.EventHandler(this.btnReturnItem_Click);
            // 
            // btnCancelReservation
            // 
            this.btnCancelReservation.Location = new System.Drawing.Point(304, 48);
            this.btnCancelReservation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCancelReservation.Name = "btnCancelReservation";
            this.btnCancelReservation.Size = new System.Drawing.Size(140, 30);
            this.btnCancelReservation.TabIndex = 7;
            this.btnCancelReservation.Text = "Anuluj rezerwacje";
            this.btnCancelReservation.UseVisualStyleBackColor = true;
            this.btnCancelReservation.Click += new System.EventHandler(this.btnCancelReservation_Click);
            // 
            // btnEditItem
            // 
            this.btnEditItem.Location = new System.Drawing.Point(12, 48);
            this.btnEditItem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnEditItem.Name = "btnEditItem";
            this.btnEditItem.Size = new System.Drawing.Size(140, 30);
            this.btnEditItem.TabIndex = 1;
            this.btnEditItem.Text = "Edytuj przedmiot";
            this.btnEditItem.UseVisualStyleBackColor = true;
            this.btnEditItem.Click += new System.EventHandler(this.btnEditItem_Click);
            // 
            // btnAddAccount
            // 
            this.btnAddAccount.Location = new System.Drawing.Point(157, 12);
            this.btnAddAccount.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAddAccount.Name = "btnAddAccount";
            this.btnAddAccount.Size = new System.Drawing.Size(140, 30);
            this.btnAddAccount.TabIndex = 3;
            this.btnAddAccount.Text = "Dodaj konto";
            this.btnAddAccount.UseVisualStyleBackColor = true;
            this.btnAddAccount.Click += new System.EventHandler(this.btnAddAccount_Click);
            // 
            // btnAddReservation
            // 
            this.btnAddReservation.Location = new System.Drawing.Point(304, 12);
            this.btnAddReservation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAddReservation.Name = "btnAddReservation";
            this.btnAddReservation.Size = new System.Drawing.Size(140, 30);
            this.btnAddReservation.TabIndex = 6;
            this.btnAddReservation.Text = "Dodaj rezerwacje";
            this.btnAddReservation.UseVisualStyleBackColor = true;
            this.btnAddReservation.Click += new System.EventHandler(this.btnAddReservation_Click);
            // 
            // btnHire
            // 
            this.btnHire.Location = new System.Drawing.Point(12, 84);
            this.btnHire.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnHire.Name = "btnHire";
            this.btnHire.Size = new System.Drawing.Size(140, 30);
            this.btnHire.TabIndex = 2;
            this.btnHire.Text = "Wypożycz";
            this.btnHire.UseVisualStyleBackColor = true;
            this.btnHire.Click += new System.EventHandler(this.btnHire_Click);
            // 
            // btnListOfHires
            // 
            this.btnListOfHires.Location = new System.Drawing.Point(805, 12);
            this.btnListOfHires.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnListOfHires.Name = "btnListOfHires";
            this.btnListOfHires.Size = new System.Drawing.Size(140, 30);
            this.btnListOfHires.TabIndex = 9;
            this.btnListOfHires.Text = "Lista wypozyczen";
            this.btnListOfHires.UseVisualStyleBackColor = true;
            this.btnListOfHires.Click += new System.EventHandler(this.btnListOfHires_Click);
            // 
            // btnEditAccount
            // 
            this.btnEditAccount.Location = new System.Drawing.Point(157, 48);
            this.btnEditAccount.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnEditAccount.Name = "btnEditAccount";
            this.btnEditAccount.Size = new System.Drawing.Size(140, 30);
            this.btnEditAccount.TabIndex = 4;
            this.btnEditAccount.Text = "Edytuj konto";
            this.btnEditAccount.UseVisualStyleBackColor = true;
            this.btnEditAccount.Click += new System.EventHandler(this.btnEditAccount_Click);
            // 
            // textBoxFiltr
            // 
            this.textBoxFiltr.Location = new System.Drawing.Point(611, 70);
            this.textBoxFiltr.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxFiltr.Name = "textBoxFiltr";
            this.textBoxFiltr.Size = new System.Drawing.Size(423, 22);
            this.textBoxFiltr.TabIndex = 10;
            this.textBoxFiltr.TextChanged += new System.EventHandler(this.textBoxFiltr_TextChanged);
            // 
            // labelFiltr
            // 
            this.labelFiltr.AutoSize = true;
            this.labelFiltr.Location = new System.Drawing.Point(549, 73);
            this.labelFiltr.Name = "labelFiltr";
            this.labelFiltr.Size = new System.Drawing.Size(35, 17);
            this.labelFiltr.TabIndex = 11;
            this.labelFiltr.Text = "Filtr:";
            // 
            // dataGridViewMain
            // 
            this.dataGridViewMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMain.Location = new System.Drawing.Point(12, 132);
            this.dataGridViewMain.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridViewMain.Name = "dataGridViewMain";
            this.dataGridViewMain.RowTemplate.Height = 24;
            this.dataGridViewMain.Size = new System.Drawing.Size(1165, 411);
            this.dataGridViewMain.TabIndex = 12;
            // 
            // btnListOfReservation
            // 
            this.btnListOfReservation.Location = new System.Drawing.Point(659, 12);
            this.btnListOfReservation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnListOfReservation.Name = "btnListOfReservation";
            this.btnListOfReservation.Size = new System.Drawing.Size(140, 30);
            this.btnListOfReservation.TabIndex = 8;
            this.btnListOfReservation.Text = "Lista rezerwacji";
            this.btnListOfReservation.UseVisualStyleBackColor = true;
            this.btnListOfReservation.Click += new System.EventHandler(this.buttonListOfReservation_Click);
            // 
            // buttonListOfArchive
            // 
            this.buttonListOfArchive.Location = new System.Drawing.Point(951, 12);
            this.buttonListOfArchive.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonListOfArchive.Name = "buttonListOfArchive";
            this.buttonListOfArchive.Size = new System.Drawing.Size(140, 30);
            this.buttonListOfArchive.TabIndex = 10;
            this.buttonListOfArchive.Text = "Archiwum";
            this.buttonListOfArchive.UseVisualStyleBackColor = true;
            this.buttonListOfArchive.Click += new System.EventHandler(this.buttonListOfArchive_Click);
            // 
            // btnListOfItems
            // 
            this.btnListOfItems.Enabled = false;
            this.btnListOfItems.Location = new System.Drawing.Point(512, 14);
            this.btnListOfItems.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnListOfItems.Name = "btnListOfItems";
            this.btnListOfItems.Size = new System.Drawing.Size(140, 28);
            this.btnListOfItems.TabIndex = 13;
            this.btnListOfItems.Text = "Lista przedmiotów";
            this.btnListOfItems.UseVisualStyleBackColor = true;
            this.btnListOfItems.Click += new System.EventHandler(this.btnListOfItems_Click);
            // 
            // MainApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1189, 554);
            this.Controls.Add(this.btnListOfItems);
            this.Controls.Add(this.buttonListOfArchive);
            this.Controls.Add(this.btnListOfReservation);
            this.Controls.Add(this.dataGridViewMain);
            this.Controls.Add(this.labelFiltr);
            this.Controls.Add(this.textBoxFiltr);
            this.Controls.Add(this.btnEditAccount);
            this.Controls.Add(this.btnListOfHires);
            this.Controls.Add(this.btnHire);
            this.Controls.Add(this.btnAddReservation);
            this.Controls.Add(this.btnAddAccount);
            this.Controls.Add(this.btnEditItem);
            this.Controls.Add(this.btnCancelReservation);
            this.Controls.Add(this.btnReturnItem);
            this.Controls.Add(this.btnAddItem);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "MainApp";
            this.Text = "Wypozyczalnia";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainApp_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMain)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAddItem;
        private System.Windows.Forms.Button btnReturnItem;
        private System.Windows.Forms.Button btnCancelReservation;
        private System.Windows.Forms.Button btnEditItem;
        private System.Windows.Forms.Button btnAddAccount;
        private System.Windows.Forms.Button btnAddReservation;
        private System.Windows.Forms.Button btnHire;
        private System.Windows.Forms.Button btnListOfHires;
        private System.Windows.Forms.Button btnEditAccount;
        private System.Windows.Forms.TextBox textBoxFiltr;
        private System.Windows.Forms.Label labelFiltr;
        private System.Windows.Forms.DataGridView dataGridViewMain;
        private System.Windows.Forms.Button btnListOfReservation;
        private System.Windows.Forms.Button buttonListOfArchive;
        private System.Windows.Forms.Button btnListOfItems;
    }
}