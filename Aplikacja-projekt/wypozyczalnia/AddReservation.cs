﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace wypozyczalnia
{
    public partial class AddReservation : Form
    {
        SqlConnection sqlConnection;
        ChoosenDatas chosenDatas = new ChoosenDatas();
        public AddReservation(SqlConnection sqlConnection)
        {
            this.sqlConnection = sqlConnection;
            InitializeComponent();
        }

        private void textBoxItemName_TextChanged(object sender, EventArgs e)
        {
            ShowRecords.ShowItems(sqlConnection, dataGridViewItem, textBoxItemName.Text, textBoxCategoryName.Text);
        }

        private void textBoxCategoryName_TextChanged(object sender, EventArgs e)
        {
            ShowRecords.ShowItems(sqlConnection, dataGridViewItem, textBoxItemName.Text, textBoxCategoryName.Text);
        }

        private void textBoxClientName_TextChanged(object sender, EventArgs e)
        {
            ShowRecords.ShowClient(sqlConnection, dataGridViewAccount, textBoxClientName.Text, textBoxClientLastName.Text);
        }

        private void textBoxClientLastName_TextChanged(object sender, EventArgs e)
        {
            ShowRecords.ShowClient(sqlConnection, dataGridViewAccount, textBoxClientName.Text, textBoxClientLastName.Text);
        }

        private void dataGridViewItem_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            chosenDatas.itemId = Int32.Parse(dataGridViewItem.Rows[dataGridViewItem.CurrentCell.RowIndex].Cells[0].Value.ToString());
            chosenDatas.itemName = dataGridViewItem.Rows[dataGridViewItem.CurrentCell.RowIndex].Cells[1].Value.ToString();
            chosenDatas.itemCategory = dataGridViewItem.Rows[dataGridViewItem.CurrentCell.RowIndex].Cells[6].Value.ToString();
            labelChosenItem.Text = "Wybrano: " + chosenDatas.itemName + " [" + chosenDatas.itemCategory + "]";
        }

        private void dataGridViewAccount_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            chosenDatas.accountId = Int32.Parse(dataGridViewAccount.Rows[dataGridViewAccount.CurrentCell.RowIndex].Cells[0].Value.ToString());;
            chosenDatas.accName = dataGridViewAccount.Rows[dataGridViewAccount.CurrentCell.RowIndex].Cells[1].Value.ToString();
            chosenDatas.accLastName = dataGridViewAccount.Rows[dataGridViewAccount.CurrentCell.RowIndex].Cells[2].Value.ToString();
            labelChooseClient.Text = "Wybrano: " + chosenDatas.accName + " " + chosenDatas.accLastName;
        }

        private void buttonAddReservation_Click(object sender, EventArgs e)
        {
            AddRecords.addNewReservation(sqlConnection, chosenDatas.itemId.ToString(), chosenDatas.accountId.ToString(), textBoxItemQtty.Text);
        }
        
    }
    public class ChoosenDatas
    {
        public int itemId { get; set; }
        public string itemName { get; set; }
        public string itemCategory { get; set; }
        public int accountId { get; set; }
        public string accName { get; set; }
        public string accLastName { get; set; }
    }
}
