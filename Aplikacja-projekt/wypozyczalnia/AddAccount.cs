﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace wypozyczalnia
{
    public partial class AddAccount : Form
    {
        SqlConnection sqlConnection;
        public AddAccount( SqlConnection sqlConnection)
        {
            this.sqlConnection = sqlConnection;
            InitializeComponent();
        }

        private void btnAddUser_Click(object sender, EventArgs e)
        {
            AddRecords.addNewAccount(sqlConnection, textBoxLogin.Text, textBoxPassword.Text, textBoxMail.Text, textBoxPhoneNumber.Text, textBoxName.Text, textBoxLastName.Text, AddRecords.addAddress(sqlConnection, textBoxPostcode.Text, textBoxBuildingNumber.Text, textBoxHouseNumber.Text, textBoxStreet.Text, textBoxCity.Text));
        }
    }
}
