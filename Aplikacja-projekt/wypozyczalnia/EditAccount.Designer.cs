﻿namespace wypozyczalnia
{
    partial class EditAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelAccLastName = new System.Windows.Forms.Label();
            this.labelIAccName = new System.Windows.Forms.Label();
            this.textBoxClientLastName = new System.Windows.Forms.TextBox();
            this.textBoxClientName = new System.Windows.Forms.TextBox();
            this.dataGridViewAccount = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAccount)).BeginInit();
            this.SuspendLayout();
            // 
            // labelAccLastName
            // 
            this.labelAccLastName.AutoSize = true;
            this.labelAccLastName.Location = new System.Drawing.Point(15, 112);
            this.labelAccLastName.Name = "labelAccLastName";
            this.labelAccLastName.Size = new System.Drawing.Size(67, 17);
            this.labelAccLastName.TabIndex = 9;
            this.labelAccLastName.Text = "Nazwisko";
            // 
            // labelIAccName
            // 
            this.labelIAccName.AutoSize = true;
            this.labelIAccName.Location = new System.Drawing.Point(15, 75);
            this.labelIAccName.Name = "labelIAccName";
            this.labelIAccName.Size = new System.Drawing.Size(37, 17);
            this.labelIAccName.TabIndex = 8;
            this.labelIAccName.Text = "Imie ";
            // 
            // textBoxClientLastName
            // 
            this.textBoxClientLastName.Location = new System.Drawing.Point(130, 109);
            this.textBoxClientLastName.Name = "textBoxClientLastName";
            this.textBoxClientLastName.Size = new System.Drawing.Size(143, 22);
            this.textBoxClientLastName.TabIndex = 7;
            this.textBoxClientLastName.TextChanged += new System.EventHandler(this.textBoxAccLastName_TextChanged);
            // 
            // textBoxClientName
            // 
            this.textBoxClientName.Location = new System.Drawing.Point(130, 72);
            this.textBoxClientName.Name = "textBoxClientName";
            this.textBoxClientName.Size = new System.Drawing.Size(143, 22);
            this.textBoxClientName.TabIndex = 6;
            this.textBoxClientName.TextChanged += new System.EventHandler(this.textBoxAccName_TextChanged);
            // 
            // dataGridViewAccount
            // 
            this.dataGridViewAccount.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewAccount.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAccount.Location = new System.Drawing.Point(16, 220);
            this.dataGridViewAccount.Name = "dataGridViewAccount";
            this.dataGridViewAccount.RowTemplate.Height = 24;
            this.dataGridViewAccount.Size = new System.Drawing.Size(1008, 283);
            this.dataGridViewAccount.TabIndex = 5;
            this.dataGridViewAccount.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewAccount_CellEndEdit);
            // 
            // EditAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1093, 524);
            this.Controls.Add(this.labelAccLastName);
            this.Controls.Add(this.labelIAccName);
            this.Controls.Add(this.textBoxClientLastName);
            this.Controls.Add(this.textBoxClientName);
            this.Controls.Add(this.dataGridViewAccount);
            this.Name = "EditAccount";
            this.Text = "Edytuj konto";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAccount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelAccLastName;
        private System.Windows.Forms.Label labelIAccName;
        private System.Windows.Forms.TextBox textBoxClientLastName;
        private System.Windows.Forms.TextBox textBoxClientName;
        private System.Windows.Forms.DataGridView dataGridViewAccount;
    }
}