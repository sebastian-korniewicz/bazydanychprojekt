﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace wypozyczalnia
{
    public partial class AddNewHire : Form
    {
        SqlConnection sqlConnection;
        public AddNewHire(SqlConnection sqlConnection)
        {
            this.sqlConnection = sqlConnection;
            InitializeComponent();

            dateTimePickerReservation.Format = DateTimePickerFormat.Custom;
            dateTimePickerReservation.CustomFormat = " ";
            update();
        }

        private void update()
        {
            if (dateTimePickerReservation.CustomFormat == " ")
            {
                ShowRecords.ShowReservation(sqlConnection, dataGridViewReservation, textBoxClientName.Text, textBoxClientLastName.Text, textBoxItemName.Text);
            }
            else
            {
                ShowRecords.ShowReservation(sqlConnection, dataGridViewReservation, textBoxClientName.Text, textBoxClientLastName.Text, textBoxItemName.Text, dateTimePickerReservation.Value.Year.ToString() + "-" + dateTimePickerReservation.Value.Month.ToString() + "-" + dateTimePickerReservation.Value.Day.ToString());
            }
        }

        private void textBoxClientName_TextChanged(object sender, EventArgs e)
        {
            update();
        }

        private void textBoxClientLastName_TextChanged(object sender, EventArgs e)
        {
            update();
        }

        private void textBoxItemName_TextChanged(object sender, EventArgs e)
        {
            update();
        }

        private void dateTimePickerReservation_ValueChanged(object sender, EventArgs e)
        {
            dateTimePickerReservation.CustomFormat = "yyyy-MM-dd";
            update();
        }

        private void dataGridViewReservation_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            dataGridViewChoosenData.Rows.Clear();
            DataGridViewColumn newCol = null;
            if (dataGridViewChoosenData.Columns.Count == 0)
            {
                foreach (DataGridViewColumn col in dataGridViewReservation.Columns)
                {
                    newCol = new DataGridViewColumn(col.CellTemplate);
                    newCol.HeaderText = col.HeaderText;
                    newCol.Name = col.Name;
                    dataGridViewChoosenData.Columns.Add(newCol);
                }
            }
            foreach (DataGridViewColumn col in dataGridViewReservation.Columns)
            {
                dataGridViewChoosenData.Rows[0].Cells[col.Name].Value = dataGridViewReservation.Rows[dataGridViewReservation.CurrentCell.RowIndex].Cells[col.Name].Value;
            }

        }

        private void buttonlAddHire_Click(object sender, EventArgs e)
        {
            AddRecords.addNewHire(sqlConnection,
                dataGridViewChoosenData.Rows[0].Cells[0].Value.ToString(),
                dataGridViewChoosenData.Rows[0].Cells[2].Value.ToString(),
                MainApp.AccountId.ToString(),
                dataGridViewChoosenData.Rows[0].Cells[6].Value.ToString(),
                dateTimePickerHire.Value.Year.ToString() + "-" + dateTimePickerHire.Value.Month.ToString() + "-" + dateTimePickerHire.Value.Day.ToString());
            dataGridViewChoosenData.Rows.Clear();
            update();
        }
    }
}
