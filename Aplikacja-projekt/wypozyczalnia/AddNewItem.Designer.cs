﻿namespace wypozyczalnia
{
    partial class AddNewItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxItemName = new System.Windows.Forms.TextBox();
            this.textBoxDeposit = new System.Windows.Forms.TextBox();
            this.textBoxHirePrice = new System.Windows.Forms.TextBox();
            this.textBoxNumberOfItems = new System.Windows.Forms.TextBox();
            this.labelItemName = new System.Windows.Forms.Label();
            this.labelDeposit = new System.Windows.Forms.Label();
            this.labelPrice = new System.Windows.Forms.Label();
            this.labelNumberOfItems = new System.Windows.Forms.Label();
            this.labelComboBoxCategory = new System.Windows.Forms.Label();
            this.comboBoxCategoryColumns = new System.Windows.Forms.ComboBox();
            this.btnAddNewItem = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxItemName
            // 
            this.textBoxItemName.Location = new System.Drawing.Point(136, 39);
            this.textBoxItemName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxItemName.Name = "textBoxItemName";
            this.textBoxItemName.Size = new System.Drawing.Size(126, 20);
            this.textBoxItemName.TabIndex = 0;
            // 
            // textBoxDeposit
            // 
            this.textBoxDeposit.Location = new System.Drawing.Point(136, 63);
            this.textBoxDeposit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxDeposit.Name = "textBoxDeposit";
            this.textBoxDeposit.Size = new System.Drawing.Size(126, 20);
            this.textBoxDeposit.TabIndex = 1;
            // 
            // textBoxHirePrice
            // 
            this.textBoxHirePrice.Location = new System.Drawing.Point(136, 87);
            this.textBoxHirePrice.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxHirePrice.Name = "textBoxHirePrice";
            this.textBoxHirePrice.Size = new System.Drawing.Size(126, 20);
            this.textBoxHirePrice.TabIndex = 2;
            // 
            // textBoxNumberOfItems
            // 
            this.textBoxNumberOfItems.Location = new System.Drawing.Point(136, 111);
            this.textBoxNumberOfItems.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxNumberOfItems.Name = "textBoxNumberOfItems";
            this.textBoxNumberOfItems.Size = new System.Drawing.Size(126, 20);
            this.textBoxNumberOfItems.TabIndex = 3;
            // 
            // labelItemName
            // 
            this.labelItemName.AutoSize = true;
            this.labelItemName.Location = new System.Drawing.Point(9, 42);
            this.labelItemName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelItemName.Name = "labelItemName";
            this.labelItemName.Size = new System.Drawing.Size(94, 13);
            this.labelItemName.TabIndex = 4;
            this.labelItemName.Text = "Nazwa przedmiotu";
            // 
            // labelDeposit
            // 
            this.labelDeposit.AutoSize = true;
            this.labelDeposit.Location = new System.Drawing.Point(9, 66);
            this.labelDeposit.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelDeposit.Name = "labelDeposit";
            this.labelDeposit.Size = new System.Drawing.Size(40, 13);
            this.labelDeposit.TabIndex = 5;
            this.labelDeposit.Text = "Kaucja";
            // 
            // labelPrice
            // 
            this.labelPrice.AutoSize = true;
            this.labelPrice.Location = new System.Drawing.Point(9, 90);
            this.labelPrice.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelPrice.Name = "labelPrice";
            this.labelPrice.Size = new System.Drawing.Size(101, 13);
            this.labelPrice.TabIndex = 6;
            this.labelPrice.Text = "Cena wypozyczenia";
            // 
            // labelNumberOfItems
            // 
            this.labelNumberOfItems.AutoSize = true;
            this.labelNumberOfItems.Location = new System.Drawing.Point(9, 114);
            this.labelNumberOfItems.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelNumberOfItems.Name = "labelNumberOfItems";
            this.labelNumberOfItems.Size = new System.Drawing.Size(100, 13);
            this.labelNumberOfItems.TabIndex = 7;
            this.labelNumberOfItems.Text = "Liczba przedmiotow";
            // 
            // labelComboBoxCategory
            // 
            this.labelComboBoxCategory.AutoSize = true;
            this.labelComboBoxCategory.Location = new System.Drawing.Point(9, 138);
            this.labelComboBoxCategory.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelComboBoxCategory.Name = "labelComboBoxCategory";
            this.labelComboBoxCategory.Size = new System.Drawing.Size(52, 13);
            this.labelComboBoxCategory.TabIndex = 8;
            this.labelComboBoxCategory.Text = "Kategoria";
            // 
            // comboBoxCategoryColumns
            // 
            this.comboBoxCategoryColumns.FormattingEnabled = true;
            this.comboBoxCategoryColumns.Location = new System.Drawing.Point(136, 135);
            this.comboBoxCategoryColumns.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboBoxCategoryColumns.Name = "comboBoxCategoryColumns";
            this.comboBoxCategoryColumns.Size = new System.Drawing.Size(126, 21);
            this.comboBoxCategoryColumns.TabIndex = 9;
            // 
            // btnAddNewItem
            // 
            this.btnAddNewItem.Location = new System.Drawing.Point(106, 169);
            this.btnAddNewItem.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnAddNewItem.Name = "btnAddNewItem";
            this.btnAddNewItem.Size = new System.Drawing.Size(156, 30);
            this.btnAddNewItem.TabIndex = 10;
            this.btnAddNewItem.Text = "Dodaj przedmiot";
            this.btnAddNewItem.UseVisualStyleBackColor = true;
            this.btnAddNewItem.Click += new System.EventHandler(this.btnAddItemForm_Click);
            // 
            // AddNewItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(278, 213);
            this.Controls.Add(this.btnAddNewItem);
            this.Controls.Add(this.comboBoxCategoryColumns);
            this.Controls.Add(this.labelComboBoxCategory);
            this.Controls.Add(this.labelNumberOfItems);
            this.Controls.Add(this.labelPrice);
            this.Controls.Add(this.labelDeposit);
            this.Controls.Add(this.labelItemName);
            this.Controls.Add(this.textBoxNumberOfItems);
            this.Controls.Add(this.textBoxHirePrice);
            this.Controls.Add(this.textBoxDeposit);
            this.Controls.Add(this.textBoxItemName);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "AddNewItem";
            this.Text = "Dodaj nowy przedmiot";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxItemName;
        private System.Windows.Forms.TextBox textBoxDeposit;
        private System.Windows.Forms.TextBox textBoxHirePrice;
        private System.Windows.Forms.TextBox textBoxNumberOfItems;
        private System.Windows.Forms.Label labelItemName;
        private System.Windows.Forms.Label labelDeposit;
        private System.Windows.Forms.Label labelPrice;
        private System.Windows.Forms.Label labelNumberOfItems;
        private System.Windows.Forms.Label labelComboBoxCategory;
        private System.Windows.Forms.ComboBox comboBoxCategoryColumns;
        private System.Windows.Forms.Button btnAddNewItem;
    }
}