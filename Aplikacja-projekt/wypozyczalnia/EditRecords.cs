﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace wypozyczalnia
{
    class EditRecords
    {
        static SqlCommand sqlCommand;
        public static void editItem(SqlConnection sqlConnection,string idPrzedmiot, string wysokoscKaucji, string cenaWypozyczenia, string liczbaPrzedmiotow,string liczbaDostepnychPrzedmiotow)
        {
            try
            {
               wysokoscKaucji= wysokoscKaucji.Replace(',', '.');
               cenaWypozyczenia= cenaWypozyczenia.Replace(',', '.');
                sqlConnection.Open();
                string query = $@"Update Przedmioty SET  wysokosc_kaucji={wysokoscKaucji},cena_wypozyczenia={cenaWypozyczenia},liczba_przedmiotow={liczbaPrzedmiotow},liczba_dostepnych_przedmiotow={liczbaDostepnychPrzedmiotow} where id_przedmiot={idPrzedmiot} ;";
                sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                MessageBox.Show("Zmieniono!");
            }
            catch (Exception e)
            {
                sqlConnection.Close();
                MessageBox.Show("Błąd danych!");
            }

        }
        public static void editAccount(SqlConnection sqlConnection, string idKonto, string imie, string nazwisko,string email,string numer_telefonu,string kod_pocztowy, string miasto, string ulica,string numer_budynku,string numer_mieszkania)
        {
            try
            {
                string query = $"select id_adres from Konta where id_konto={idKonto} ";
                decimal id_adres;

                sqlConnection.Open();
                using (var command = new SqlCommand(query, sqlConnection))
                {
                    id_adres = (decimal)command.ExecuteScalar();
                }
                 query = $@"Update Konta SET  imie='{imie}',nazwisko='{nazwisko}',email='{email}',nr_telefonu='{numer_telefonu}' where id_konto={idKonto} ;";
                sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.ExecuteNonQuery();
                query = $@"Update Adresy SET  kod_pocztowy='{kod_pocztowy}',miasto='{miasto}',ulica='{ulica}',numer_budynku='{numer_budynku}',numer_mieszkania='{numer_mieszkania}' where id_adres={id_adres} ;";
                sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                MessageBox.Show("Zmieniono!");
            }
            catch (Exception e)
            {
                sqlConnection.Close();
                MessageBox.Show("Błąd danych!");
            }

        }
    }
}
