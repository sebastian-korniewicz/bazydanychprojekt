﻿namespace wypozyczalnia
{
    partial class AddNewHire
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonlAddHire = new System.Windows.Forms.Button();
            this.labelChoosenData = new System.Windows.Forms.Label();
            this.dataGridViewChoosenData = new System.Windows.Forms.DataGridView();
            this.labelClientLastNAme = new System.Windows.Forms.Label();
            this.labelClientName = new System.Windows.Forms.Label();
            this.textBoxClientLastName = new System.Windows.Forms.TextBox();
            this.textBoxClientName = new System.Windows.Forms.TextBox();
            this.dataGridViewReservation = new System.Windows.Forms.DataGridView();
            this.labelDateTimePickerHire = new System.Windows.Forms.Label();
            this.dateTimePickerHire = new System.Windows.Forms.DateTimePicker();
            this.labelItemName = new System.Windows.Forms.Label();
            this.textBoxItemName = new System.Windows.Forms.TextBox();
            this.labelDateTimePicker = new System.Windows.Forms.Label();
            this.dateTimePickerReservation = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewChoosenData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReservation)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonlAddHire
            // 
            this.buttonlAddHire.Location = new System.Drawing.Point(652, 434);
            this.buttonlAddHire.Margin = new System.Windows.Forms.Padding(2);
            this.buttonlAddHire.Name = "buttonlAddHire";
            this.buttonlAddHire.Size = new System.Drawing.Size(99, 37);
            this.buttonlAddHire.TabIndex = 23;
            this.buttonlAddHire.Text = "Dodaj wypozyczenie";
            this.buttonlAddHire.UseVisualStyleBackColor = true;
            this.buttonlAddHire.Click += new System.EventHandler(this.buttonlAddHire_Click);
            // 
            // labelChoosenData
            // 
            this.labelChoosenData.AutoSize = true;
            this.labelChoosenData.Location = new System.Drawing.Point(8, 355);
            this.labelChoosenData.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelChoosenData.Name = "labelChoosenData";
            this.labelChoosenData.Size = new System.Drawing.Size(53, 13);
            this.labelChoosenData.TabIndex = 22;
            this.labelChoosenData.Text = "Wybrano:";
            // 
            // dataGridViewChoosenData
            // 
            this.dataGridViewChoosenData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewChoosenData.Location = new System.Drawing.Point(8, 379);
            this.dataGridViewChoosenData.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridViewChoosenData.Name = "dataGridViewChoosenData";
            this.dataGridViewChoosenData.RowTemplate.Height = 24;
            this.dataGridViewChoosenData.Size = new System.Drawing.Size(743, 46);
            this.dataGridViewChoosenData.TabIndex = 21;
            // 
            // labelClientLastNAme
            // 
            this.labelClientLastNAme.AutoSize = true;
            this.labelClientLastNAme.Location = new System.Drawing.Point(52, 32);
            this.labelClientLastNAme.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelClientLastNAme.Name = "labelClientLastNAme";
            this.labelClientLastNAme.Size = new System.Drawing.Size(53, 13);
            this.labelClientLastNAme.TabIndex = 18;
            this.labelClientLastNAme.Text = "Nazwisko";
            // 
            // labelClientName
            // 
            this.labelClientName.AutoSize = true;
            this.labelClientName.Location = new System.Drawing.Point(52, 12);
            this.labelClientName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelClientName.Name = "labelClientName";
            this.labelClientName.Size = new System.Drawing.Size(26, 13);
            this.labelClientName.TabIndex = 17;
            this.labelClientName.Text = "Imie";
            // 
            // textBoxClientLastName
            // 
            this.textBoxClientLastName.Location = new System.Drawing.Point(118, 32);
            this.textBoxClientLastName.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxClientLastName.Name = "textBoxClientLastName";
            this.textBoxClientLastName.Size = new System.Drawing.Size(151, 20);
            this.textBoxClientLastName.TabIndex = 15;
            this.textBoxClientLastName.TextChanged += new System.EventHandler(this.textBoxClientLastName_TextChanged);
            // 
            // textBoxClientName
            // 
            this.textBoxClientName.Location = new System.Drawing.Point(118, 10);
            this.textBoxClientName.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxClientName.Name = "textBoxClientName";
            this.textBoxClientName.Size = new System.Drawing.Size(151, 20);
            this.textBoxClientName.TabIndex = 14;
            this.textBoxClientName.TextChanged += new System.EventHandler(this.textBoxClientName_TextChanged);
            // 
            // dataGridViewReservation
            // 
            this.dataGridViewReservation.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewReservation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewReservation.Location = new System.Drawing.Point(8, 63);
            this.dataGridViewReservation.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridViewReservation.Name = "dataGridViewReservation";
            this.dataGridViewReservation.RowTemplate.Height = 24;
            this.dataGridViewReservation.Size = new System.Drawing.Size(743, 283);
            this.dataGridViewReservation.TabIndex = 12;
            this.dataGridViewReservation.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewReservation_CellClick);
            // 
            // labelDateTimePickerHire
            // 
            this.labelDateTimePickerHire.AutoSize = true;
            this.labelDateTimePickerHire.Location = new System.Drawing.Point(10, 450);
            this.labelDateTimePickerHire.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelDateTimePickerHire.Name = "labelDateTimePickerHire";
            this.labelDateTimePickerHire.Size = new System.Drawing.Size(131, 13);
            this.labelDateTimePickerHire.TabIndex = 25;
            this.labelDateTimePickerHire.Text = "Data planowanego zwrotu";
            // 
            // dateTimePickerHire
            // 
            this.dateTimePickerHire.Location = new System.Drawing.Point(152, 450);
            this.dateTimePickerHire.Margin = new System.Windows.Forms.Padding(2);
            this.dateTimePickerHire.Name = "dateTimePickerHire";
            this.dateTimePickerHire.Size = new System.Drawing.Size(151, 20);
            this.dateTimePickerHire.TabIndex = 24;
            // 
            // labelItemName
            // 
            this.labelItemName.AutoSize = true;
            this.labelItemName.Location = new System.Drawing.Point(343, 10);
            this.labelItemName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelItemName.Name = "labelItemName";
            this.labelItemName.Size = new System.Drawing.Size(94, 13);
            this.labelItemName.TabIndex = 19;
            this.labelItemName.Text = "Nazwa przedmiotu";
            // 
            // textBoxItemName
            // 
            this.textBoxItemName.Location = new System.Drawing.Point(440, 10);
            this.textBoxItemName.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxItemName.Name = "textBoxItemName";
            this.textBoxItemName.Size = new System.Drawing.Size(151, 20);
            this.textBoxItemName.TabIndex = 16;
            this.textBoxItemName.TextChanged += new System.EventHandler(this.textBoxItemName_TextChanged);
            // 
            // labelDateTimePicker
            // 
            this.labelDateTimePicker.AutoSize = true;
            this.labelDateTimePicker.Location = new System.Drawing.Point(343, 32);
            this.labelDateTimePicker.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelDateTimePicker.Name = "labelDateTimePicker";
            this.labelDateTimePicker.Size = new System.Drawing.Size(30, 13);
            this.labelDateTimePicker.TabIndex = 20;
            this.labelDateTimePicker.Text = "Data";
            // 
            // dateTimePickerReservation
            // 
            this.dateTimePickerReservation.Location = new System.Drawing.Point(440, 32);
            this.dateTimePickerReservation.Margin = new System.Windows.Forms.Padding(2);
            this.dateTimePickerReservation.Name = "dateTimePickerReservation";
            this.dateTimePickerReservation.Size = new System.Drawing.Size(151, 20);
            this.dateTimePickerReservation.TabIndex = 13;
            this.dateTimePickerReservation.ValueChanged += new System.EventHandler(this.dateTimePickerReservation_ValueChanged);
            // 
            // AddNewHire
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(756, 478);
            this.Controls.Add(this.labelDateTimePickerHire);
            this.Controls.Add(this.dateTimePickerHire);
            this.Controls.Add(this.buttonlAddHire);
            this.Controls.Add(this.labelChoosenData);
            this.Controls.Add(this.dataGridViewChoosenData);
            this.Controls.Add(this.labelDateTimePicker);
            this.Controls.Add(this.labelItemName);
            this.Controls.Add(this.labelClientLastNAme);
            this.Controls.Add(this.labelClientName);
            this.Controls.Add(this.textBoxItemName);
            this.Controls.Add(this.textBoxClientLastName);
            this.Controls.Add(this.textBoxClientName);
            this.Controls.Add(this.dateTimePickerReservation);
            this.Controls.Add(this.dataGridViewReservation);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "AddNewHire";
            this.Text = "Dodaj nowe wypożyczenie";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewChoosenData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReservation)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonlAddHire;
        private System.Windows.Forms.Label labelChoosenData;
        private System.Windows.Forms.DataGridView dataGridViewChoosenData;
        private System.Windows.Forms.Label labelClientLastNAme;
        private System.Windows.Forms.Label labelClientName;
        private System.Windows.Forms.TextBox textBoxClientLastName;
        private System.Windows.Forms.TextBox textBoxClientName;
        private System.Windows.Forms.DataGridView dataGridViewReservation;
        private System.Windows.Forms.Label labelDateTimePickerHire;
        private System.Windows.Forms.DateTimePicker dateTimePickerHire;
        private System.Windows.Forms.Label labelItemName;
        private System.Windows.Forms.TextBox textBoxItemName;
        private System.Windows.Forms.Label labelDateTimePicker;
        private System.Windows.Forms.DateTimePicker dateTimePickerReservation;
    }
}