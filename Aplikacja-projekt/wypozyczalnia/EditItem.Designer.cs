﻿namespace wypozyczalnia
{
    partial class EditItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewItem = new System.Windows.Forms.DataGridView();
            this.textBoxItemName = new System.Windows.Forms.TextBox();
            this.textBoxItemCategory = new System.Windows.Forms.TextBox();
            this.labelItemName = new System.Windows.Forms.Label();
            this.labelItemCategory = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewItem)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewItem
            // 
            this.dataGridViewItem.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewItem.Location = new System.Drawing.Point(13, 186);
            this.dataGridViewItem.Name = "dataGridViewItem";
            this.dataGridViewItem.RowTemplate.Height = 24;
            this.dataGridViewItem.Size = new System.Drawing.Size(1045, 283);
            this.dataGridViewItem.TabIndex = 0;
            this.dataGridViewItem.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewItem_CellEndEdit);
            // 
            // textBoxItemName
            // 
            this.textBoxItemName.Location = new System.Drawing.Point(170, 38);
            this.textBoxItemName.Name = "textBoxItemName";
            this.textBoxItemName.Size = new System.Drawing.Size(137, 22);
            this.textBoxItemName.TabIndex = 1;
            this.textBoxItemName.TextChanged += new System.EventHandler(this.textBoxItemName_TextChanged);
            // 
            // textBoxItemCategory
            // 
            this.textBoxItemCategory.Location = new System.Drawing.Point(170, 75);
            this.textBoxItemCategory.Name = "textBoxItemCategory";
            this.textBoxItemCategory.Size = new System.Drawing.Size(137, 22);
            this.textBoxItemCategory.TabIndex = 2;
            this.textBoxItemCategory.TextChanged += new System.EventHandler(this.textBoxItemCategory_TextChanged);
            // 
            // labelItemName
            // 
            this.labelItemName.AutoSize = true;
            this.labelItemName.Location = new System.Drawing.Point(12, 41);
            this.labelItemName.Name = "labelItemName";
            this.labelItemName.Size = new System.Drawing.Size(124, 17);
            this.labelItemName.TabIndex = 3;
            this.labelItemName.Text = "Nazwa przedmiotu";
            // 
            // labelItemCategory
            // 
            this.labelItemCategory.AutoSize = true;
            this.labelItemCategory.Location = new System.Drawing.Point(12, 78);
            this.labelItemCategory.Name = "labelItemCategory";
            this.labelItemCategory.Size = new System.Drawing.Size(143, 17);
            this.labelItemCategory.TabIndex = 4;
            this.labelItemCategory.Text = "Kategoria przedmiotu";
            // 
            // EditItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1033, 481);
            this.Controls.Add(this.labelItemCategory);
            this.Controls.Add(this.labelItemName);
            this.Controls.Add(this.textBoxItemCategory);
            this.Controls.Add(this.textBoxItemName);
            this.Controls.Add(this.dataGridViewItem);
            this.Name = "EditItem";
            this.Text = "Edytuj przedmiot";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewItem)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewItem;
        private System.Windows.Forms.TextBox textBoxItemName;
        private System.Windows.Forms.TextBox textBoxItemCategory;
        private System.Windows.Forms.Label labelItemName;
        private System.Windows.Forms.Label labelItemCategory;
    }
}