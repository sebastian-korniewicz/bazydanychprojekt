﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace wypozyczalnia
{
    public partial class EditAccount : Form
    {
        SqlConnection sqlConnection;
        public EditAccount(SqlConnection sqlConnection)
        {
            this.sqlConnection = sqlConnection;
            InitializeComponent();
        }

        private void textBoxAccName_TextChanged(object sender, EventArgs e)
        {
            ShowRecords.ShowClient(sqlConnection, dataGridViewAccount, textBoxClientName.Text, textBoxClientLastName.Text);
            dataGridViewAccount.Columns[0].ReadOnly = true;
        }

        private void textBoxAccLastName_TextChanged(object sender, EventArgs e)
        {
            ShowRecords.ShowClient(sqlConnection, dataGridViewAccount, textBoxClientName.Text, textBoxClientLastName.Text);
            dataGridViewAccount.Columns[0].ReadOnly = true;
        }

        private void dataGridViewAccount_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            EditRecords.editAccount(sqlConnection,
                dataGridViewAccount.Rows[e.RowIndex].Cells[0].Value.ToString(),
                dataGridViewAccount.Rows[e.RowIndex].Cells[1].Value.ToString(),
                dataGridViewAccount.Rows[e.RowIndex].Cells[2].Value.ToString(),
                dataGridViewAccount.Rows[e.RowIndex].Cells[3].Value.ToString(),
                dataGridViewAccount.Rows[e.RowIndex].Cells[4].Value.ToString(),
                dataGridViewAccount.Rows[e.RowIndex].Cells[5].Value.ToString(),
                dataGridViewAccount.Rows[e.RowIndex].Cells[6].Value.ToString(),
                dataGridViewAccount.Rows[e.RowIndex].Cells[7].Value.ToString(),
                dataGridViewAccount.Rows[e.RowIndex].Cells[8].Value.ToString(),
                dataGridViewAccount.Rows[e.RowIndex].Cells[9].Value.ToString());
            ShowRecords.ShowClient(sqlConnection, dataGridViewAccount, textBoxClientName.Text, textBoxClientLastName.Text);
        }
    }
}
