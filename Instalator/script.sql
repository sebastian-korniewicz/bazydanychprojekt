USE [master]
GO
/****** Object:  Database [wypozyczalnia]    Script Date: 09.01.2018 10:02:07 ******/
CREATE DATABASE [wypozyczalnia]
 CONTAINMENT = NONE
 GO
ALTER DATABASE [wypozyczalnia] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [wypozyczalnia].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [wypozyczalnia] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [wypozyczalnia] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [wypozyczalnia] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [wypozyczalnia] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [wypozyczalnia] SET ARITHABORT OFF 
GO
ALTER DATABASE [wypozyczalnia] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [wypozyczalnia] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [wypozyczalnia] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [wypozyczalnia] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [wypozyczalnia] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [wypozyczalnia] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [wypozyczalnia] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [wypozyczalnia] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [wypozyczalnia] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [wypozyczalnia] SET  DISABLE_BROKER 
GO
ALTER DATABASE [wypozyczalnia] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [wypozyczalnia] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [wypozyczalnia] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [wypozyczalnia] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [wypozyczalnia] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [wypozyczalnia] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [wypozyczalnia] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [wypozyczalnia] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [wypozyczalnia] SET  MULTI_USER 
GO
ALTER DATABASE [wypozyczalnia] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [wypozyczalnia] SET DB_CHAINING OFF 
GO
ALTER DATABASE [wypozyczalnia] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [wypozyczalnia] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [wypozyczalnia] SET DELAYED_DURABILITY = DISABLED 
GO
USE [wypozyczalnia]
GO


/****** Object:  UserDefinedFunction [dbo].[f_liczba_dostepnych_przemiotow]    Script Date: 09.01.2018 10:02:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[f_liczba_dostepnych_przemiotow](@id int)
returns int
as begin
    return (select liczba_dostepnych_przedmiotow from przedmioty where id_przedmiot = @id)
end



GO
/****** Object:  UserDefinedFunction [dbo].[f_liczba_przemiotow]    Script Date: 09.01.2018 10:02:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[f_liczba_przemiotow](@id int)
returns int
as begin
    return (select liczba_przedmiotow from przedmioty where id_przedmiot = @id)
end



GO
/****** Object:  Table [dbo].[Adresy]    Script Date: 09.01.2018 10:02:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Adresy](
	[id_adres] [decimal](5, 0) IDENTITY(1,1) NOT NULL,
	[kod_pocztowy] [decimal](5, 0) NOT NULL,
	[numer_budynku] [nvarchar](10) NOT NULL,
	[numer_mieszkania] [nvarchar](10) NULL,
	[ulica] [nvarchar](50) NULL,
	[miasto] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_adres] PRIMARY KEY CLUSTERED 
(
	[id_adres] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Archiwalne_wypozyczenia]    Script Date: 09.01.2018 10:02:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Archiwalne_wypozyczenia](
	[id_wypozyczenie_archiwum] [decimal](7, 0) IDENTITY(1,1) NOT NULL,
	[id_konto] [decimal](5, 0) NOT NULL,
	[id_przedmiot] [decimal](5, 0) NOT NULL,
	[id_pracownika_wypozyczajacego] [decimal](5, 0) NOT NULL,
	[id_pracownika_odbierajacego] [decimal](5, 0) NOT NULL,
	[data_wypozyczenia] [date] NOT NULL,
	[data_planowanego_zwrotu] [date] NOT NULL,
	[data_zwrotu] [date] NOT NULL,
 CONSTRAINT [PK_archiwum_wypozyczen] PRIMARY KEY CLUSTERED 
(
	[id_wypozyczenie_archiwum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Kategorie]    Script Date: 09.01.2018 10:02:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Kategorie](
	[id_kategoria] [decimal](3, 0) IDENTITY(1,1) NOT NULL,
	[nazwa] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_kategoria] PRIMARY KEY CLUSTERED 
(
	[id_kategoria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[nazwa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Konta]    Script Date: 09.01.2018 10:02:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Konta](
	[id_konto] [decimal](5, 0) IDENTITY(1,1) NOT NULL,
	[login] [nvarchar](50) NOT NULL,
	[haslo] [nvarchar](50) NOT NULL,
	[id_adres] [decimal](5, 0) NOT NULL,
	[data_rejestracji] [date] NOT NULL,
	[email] [nvarchar](50) NOT NULL,
	[nr_telefonu] [nvarchar](15) NOT NULL,
	[imie] [nvarchar](50) NOT NULL,
	[nazwisko] [nvarchar](50) NOT NULL,
	[rodzaj_konta] [decimal](1, 0) NOT NULL,
 CONSTRAINT [PK_konto] PRIMARY KEY CLUSTERED 
(
	[id_konto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[login] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Przedmioty]    Script Date: 09.01.2018 10:02:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Przedmioty](
	[id_przedmiot] [decimal](5, 0) IDENTITY(1,1) NOT NULL,
	[nazwa] [nvarchar](50) NOT NULL,
	[wysokosc_kaucji] [decimal](6, 2) NOT NULL,
	[cena_wypozyczenia] [decimal](6, 2) NOT NULL,
	[liczba_przedmiotow] [decimal](4, 0) NOT NULL,
	[liczba_dostepnych_przedmiotow] [decimal](4, 0) NOT NULL,
	[id_kategoria] [decimal](3, 0) NOT NULL,
 CONSTRAINT [PK_przedmiot] PRIMARY KEY CLUSTERED 
(
	[id_przedmiot] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Rezerwacje]    Script Date: 09.01.2018 10:02:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rezerwacje](
	[id_przedmiot] [decimal](5, 0) NOT NULL,
	[id_konto] [decimal](5, 0) NOT NULL,
	[data_stworzenie_rezerwacji] [datetime] NOT NULL,
	[data_rezerwacji] [date] NULL,
	[liczba_przedmiotow] [decimal](4, 0) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Wypozyczenia]    Script Date: 09.01.2018 10:02:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Wypozyczenia](
	[id_wypozyczenie] [decimal](7, 0) IDENTITY(1,1) NOT NULL,
	[id_przedmiot] [decimal](5, 0) NOT NULL,
	[id_konto] [decimal](5, 0) NOT NULL,
	[id_pracownika_wypozyczajacego] [decimal](5, 0) NOT NULL,
	[liczba_przedmiotow] [decimal](4, 0) NOT NULL,
	[data_wypozyczenia] [date] NOT NULL,
	[data_planowanego_zwrotu] [date] NOT NULL,
 CONSTRAINT [PK_wypozyczenie] PRIMARY KEY CLUSTERED 
(
	[id_wypozyczenie] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[ArchiwalneWypozyczenia]    Script Date: 09.01.2018 10:02:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ArchiwalneWypozyczenia] AS


SELECT A.id_wypozyczenie AS "Id wypozyczenia" , A.data_wypozyczenia,  A.data_planowanego_zwrotu, B.id_konto AS "Id konta", B.imie, B.nazwisko,C.nazwa AS "nazwa przedmiotu" ,A.liczba_przedmiotow  

FROM Wypozyczenia A inner join  Konta B ON (A.id_konto=B.id_konto) inner join Przedmioty C ON (A.id_przedmiot=C.id_przedmiot)



GO
/****** Object:  View [dbo].[ArchiwalneWypozyczenie]    Script Date: 09.01.2018 10:02:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create VIEW [dbo].[ArchiwalneWypozyczenie]
AS
SELECT C.nazwa,B.imie, B.nazwisko,B.email,B.nr_telefonu,A.data_wypozyczenia,A.data_planowanego_zwrotu,A.data_zwrotu, A.id_pracownika_wypozyczajacego,
      A.id_pracownika_odbierajacego 

FROM Archiwalne_wypozyczenia A inner join  Konta B ON (A.id_konto=B.id_konto) inner join Przedmioty C ON (A.id_przedmiot=C.id_przedmiot) 



GO
/****** Object:  View [dbo].[Konto]    Script Date: 09.01.2018 10:02:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Konto] AS

SELECT A.id_konto AS "Id konta" , A.imie, A.nazwisko,A.email,A.nr_telefonu,B.kod_pocztowy, B.miasto,
B.ulica, B.numer_budynku, B.numer_mieszkania

FROM Konta A inner join  Adresy B ON (A.id_adres=B.id_adres)



GO
/****** Object:  View [dbo].[Przedmiot]    Script Date: 09.01.2018 10:02:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Przedmiot] AS

SELECT A.id_przedmiot AS "Id przedmiotu", A.nazwa, A.wysokosc_kaucji, A.cena_wypozyczenia,
A.liczba_przedmiotow, A.liczba_dostepnych_przedmiotow,B.nazwa AS "nazwa kategorii"

FROM Przedmioty A inner join  Kategorie B ON (A.id_kategoria=B.id_kategoria)



GO
/****** Object:  View [dbo].[Rezerwacja]    Script Date: 09.01.2018 10:02:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Rezerwacja] AS
SELECT A.id_przedmiot AS "Id przemiotu", A.data_rezerwacji AS "Data rezerwacji",B.id_konto, B.imie, B.nazwisko,C.nazwa,A.liczba_przedmiotow
FROM (Rezerwacje A inner join  Konta B  ON (A.id_konto=B.id_konto)) inner join Przedmioty C ON(A.id_przedmiot=C.id_przedmiot)




GO
/****** Object:  View [dbo].[Wypozyczenie]    Script Date: 09.01.2018 10:02:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Wypozyczenie] AS


SELECT A.id_wypozyczenie AS "Id wypozyczenia" , A.data_wypozyczenia,  A.data_planowanego_zwrotu, B.id_konto AS "Id konta", B.imie, B.nazwisko,C.nazwa AS "nazwa przedmiotu" ,A.liczba_przedmiotow  

FROM Wypozyczenia A inner join  Konta B ON (A.id_konto=B.id_konto) inner join Przedmioty C ON (A.id_przedmiot=C.id_przedmiot)



GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [idx_imie]    Script Date: 09.01.2018 10:02:07 ******/
CREATE NONCLUSTERED INDEX [idx_imie] ON [dbo].[Konta]
(
	[imie] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [idx_nazwisko]    Script Date: 09.01.2018 10:02:07 ******/
CREATE NONCLUSTERED INDEX [idx_nazwisko] ON [dbo].[Konta]
(
	[nazwisko] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [idx_przedmiot]    Script Date: 09.01.2018 10:02:07 ******/
CREATE NONCLUSTERED INDEX [idx_przedmiot] ON [dbo].[Przedmioty]
(
	[nazwa] ASC,
	[id_kategoria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [idx_rezerwacja]    Script Date: 09.01.2018 10:02:07 ******/
CREATE NONCLUSTERED INDEX [idx_rezerwacja] ON [dbo].[Rezerwacje]
(
	[id_konto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Archiwalne_wypozyczenia]  WITH CHECK ADD  CONSTRAINT [FK_archiwum_konta] FOREIGN KEY([id_konto])
REFERENCES [dbo].[Konta] ([id_konto])
GO
ALTER TABLE [dbo].[Archiwalne_wypozyczenia] CHECK CONSTRAINT [FK_archiwum_konta]
GO
ALTER TABLE [dbo].[Archiwalne_wypozyczenia]  WITH CHECK ADD  CONSTRAINT [FK_archiwum_pracownik_o] FOREIGN KEY([id_pracownika_odbierajacego])
REFERENCES [dbo].[Konta] ([id_konto])
GO
ALTER TABLE [dbo].[Archiwalne_wypozyczenia] CHECK CONSTRAINT [FK_archiwum_pracownik_o]
GO
ALTER TABLE [dbo].[Archiwalne_wypozyczenia]  WITH CHECK ADD  CONSTRAINT [FK_archiwum_pracownik_w] FOREIGN KEY([id_pracownika_wypozyczajacego])
REFERENCES [dbo].[Konta] ([id_konto])
GO
ALTER TABLE [dbo].[Archiwalne_wypozyczenia] CHECK CONSTRAINT [FK_archiwum_pracownik_w]
GO
ALTER TABLE [dbo].[Archiwalne_wypozyczenia]  WITH CHECK ADD  CONSTRAINT [FK_archiwum_przedmioty] FOREIGN KEY([id_przedmiot])
REFERENCES [dbo].[Przedmioty] ([id_przedmiot])
GO
ALTER TABLE [dbo].[Archiwalne_wypozyczenia] CHECK CONSTRAINT [FK_archiwum_przedmioty]
GO
ALTER TABLE [dbo].[Konta]  WITH CHECK ADD  CONSTRAINT [FK_konta_adresy] FOREIGN KEY([id_adres])
REFERENCES [dbo].[Adresy] ([id_adres])
GO
ALTER TABLE [dbo].[Konta] CHECK CONSTRAINT [FK_konta_adresy]
GO
ALTER TABLE [dbo].[Przedmioty]  WITH CHECK ADD  CONSTRAINT [FK_przedmioty_kategirie] FOREIGN KEY([id_kategoria])
REFERENCES [dbo].[Kategorie] ([id_kategoria])
GO
ALTER TABLE [dbo].[Przedmioty] CHECK CONSTRAINT [FK_przedmioty_kategirie]
GO
ALTER TABLE [dbo].[Rezerwacje]  WITH CHECK ADD  CONSTRAINT [FK_rezerwacje_konta] FOREIGN KEY([id_konto])
REFERENCES [dbo].[Konta] ([id_konto])
GO
ALTER TABLE [dbo].[Rezerwacje] CHECK CONSTRAINT [FK_rezerwacje_konta]
GO
ALTER TABLE [dbo].[Rezerwacje]  WITH CHECK ADD  CONSTRAINT [FK_rezerwacje_przedmioty] FOREIGN KEY([id_przedmiot])
REFERENCES [dbo].[Przedmioty] ([id_przedmiot])
GO
ALTER TABLE [dbo].[Rezerwacje] CHECK CONSTRAINT [FK_rezerwacje_przedmioty]
GO
ALTER TABLE [dbo].[Wypozyczenia]  WITH CHECK ADD  CONSTRAINT [FK_wypozyczenia_konta] FOREIGN KEY([id_konto])
REFERENCES [dbo].[Konta] ([id_konto])
GO
ALTER TABLE [dbo].[Wypozyczenia] CHECK CONSTRAINT [FK_wypozyczenia_konta]
GO
ALTER TABLE [dbo].[Wypozyczenia]  WITH CHECK ADD  CONSTRAINT [FK_wypozyczenia_przedmioty] FOREIGN KEY([id_przedmiot])
REFERENCES [dbo].[Przedmioty] ([id_przedmiot])
GO
ALTER TABLE [dbo].[Wypozyczenia] CHECK CONSTRAINT [FK_wypozyczenia_przedmioty]
GO
ALTER TABLE [dbo].[Przedmioty]  WITH CHECK ADD  CONSTRAINT [sprawdzenie_ceny] CHECK  (([cena_wypozyczenia]>(0)))
GO
ALTER TABLE [dbo].[Przedmioty] CHECK CONSTRAINT [sprawdzenie_ceny]
GO
ALTER TABLE [dbo].[Przedmioty]  WITH CHECK ADD  CONSTRAINT [sprawdzenie_kaucji] CHECK  (([cena_wypozyczenia]>=(0)))
GO
ALTER TABLE [dbo].[Przedmioty] CHECK CONSTRAINT [sprawdzenie_kaucji]
GO
ALTER TABLE [dbo].[Przedmioty]  WITH CHECK ADD  CONSTRAINT [sprawdzenie_liczby_dostepnych_przedmiotow] CHECK  (([liczba_przedmiotow]>=[liczba_dostepnych_przedmiotow]))
GO
ALTER TABLE [dbo].[Przedmioty] CHECK CONSTRAINT [sprawdzenie_liczby_dostepnych_przedmiotow]
GO
ALTER TABLE [dbo].[Przedmioty]  WITH CHECK ADD  CONSTRAINT [sprawdzenie_liczby_dostepnych_przedmiotow0] CHECK  (([liczba_dostepnych_przedmiotow]>=(0)))
GO
ALTER TABLE [dbo].[Przedmioty] CHECK CONSTRAINT [sprawdzenie_liczby_dostepnych_przedmiotow0]
GO
ALTER TABLE [dbo].[Przedmioty]  WITH CHECK ADD  CONSTRAINT [sprawdzenie_liczby_przedmiotow] CHECK  (([liczba_przedmiotow]>(0)))
GO
ALTER TABLE [dbo].[Przedmioty] CHECK CONSTRAINT [sprawdzenie_liczby_przedmiotow]
GO
ALTER TABLE [dbo].[Rezerwacje]  WITH CHECK ADD  CONSTRAINT [rezerwacje_sprawdzenie_liczby_przedmiotow] CHECK  (([liczba_przedmiotow]<=[dbo].[f_liczba_przemiotow]([id_przedmiot])))
GO
ALTER TABLE [dbo].[Rezerwacje] CHECK CONSTRAINT [rezerwacje_sprawdzenie_liczby_przedmiotow]
GO
ALTER TABLE [dbo].[Rezerwacje]  WITH CHECK ADD  CONSTRAINT [rezerwacje_sprawdzenie_liczby_przedmiotow0] CHECK  (([liczba_przedmiotow]>(0)))
GO
ALTER TABLE [dbo].[Rezerwacje] CHECK CONSTRAINT [rezerwacje_sprawdzenie_liczby_przedmiotow0]
GO
ALTER TABLE [dbo].[Wypozyczenia]  WITH CHECK ADD  CONSTRAINT [data] CHECK  (([data_wypozyczenia]<=[data_planowanego_zwrotu]))
GO
ALTER TABLE [dbo].[Wypozyczenia] CHECK CONSTRAINT [data]
GO
ALTER TABLE [dbo].[Wypozyczenia]  WITH CHECK ADD  CONSTRAINT [liczba_przedmiotow] CHECK  (([liczba_przedmiotow]>(0)))
GO
ALTER TABLE [dbo].[Wypozyczenia] CHECK CONSTRAINT [liczba_przedmiotow]
GO
ALTER TABLE [dbo].[Wypozyczenia]  WITH CHECK ADD  CONSTRAINT [liczba_przedmiotow0] CHECK  (([liczba_przedmiotow]<=[dbo].[f_liczba_przemiotow]([id_przedmiot])))
GO
ALTER TABLE [dbo].[Wypozyczenia] CHECK CONSTRAINT [liczba_przedmiotow0]
GO
USE [master]
GO
ALTER DATABASE [wypozyczalnia] SET  READ_WRITE 
GO
USE [wypozyczalnia]
GO
create LOGIN pracownik  
    WITH PASSWORD = 'a';  
GO  
create LOGIN admin   
    WITH PASSWORD = 'admin';  
GO  
create LOGIN klient   
    WITH PASSWORD = 'klient';  
GO  

create USER pracownik FOR LOGIN pracownik;  
GO
create USER admin FOR LOGIN admin;  
GO
create USER klient FOR LOGIN klient;  
GO

declare @sql_command varchar(max);
set @sql_command = '';
SELECT @sql_command += 'GRANT SELECT ON ' + TABLE_NAME + ' TO klient,pracownik,admin  '
FROM INFORMATION_SCHEMA.TABLES 
exec (@sql_command);
go
declare @sql_command varchar(max);
set @sql_command = '';
SELECT @sql_command += 'GRANT DELETE ON ' + TABLE_NAME + ' TO pracownik,admin  '
FROM INFORMATION_SCHEMA.TABLES WHERE table_name!='Archiwalne_wypozyczenia'
exec (@sql_command);
GRANT DELETE ON [dbo].[REZERWACJE] TO klient;
go
declare @sql_command varchar(max);
set @sql_command = '';
SELECT @sql_command += 'GRANT UPDATE ON ' + TABLE_NAME + ' TO pracownik, admin  '
FROM INFORMATION_SCHEMA.TABLES WHERE (table_name='przedmioty' OR table_name='konta')
exec (@sql_command);
go
declare @sql_command varchar(max);
set @sql_command = '';
SELECT @sql_command += 'GRANT INSERT ON ' + TABLE_NAME + ' TO pracownik, admin  '
FROM INFORMATION_SCHEMA.TABLES WHERE (table_name!='archiwum' OR table_name='kategorie')
set @sql_command = @sql_command +' GRANT INSERT ON kategorie to admin'
set @sql_command = @sql_command +' GRANT INSERT ON rezerwacje to klient'
exec (@sql_command);
go

insert into Adresy (kod_pocztowy, numer_budynku, numer_mieszkania, ulica, miasto) values (00000, 0, 0, 'Ulica', 'Miasto')
GO
insert into Konta (login, haslo, id_adres, data_rejestracji, email, nr_telefonu, imie, nazwisko, rodzaj_konta) values ('klient', 'klient', 1, '2016-10-19 11:34:40', 'email', '000-00-00', 'Sebastian', 'Klient', 0)
GO
insert into Konta (login, haslo, id_adres, data_rejestracji, email, nr_telefonu, imie, nazwisko, rodzaj_konta) values ('pracownik', 'pracownik', 1, '2016-10-19 11:34:40', 'email', '000-00-00', 'Sebastian', 'Pracownik', 1)
GO
insert into Konta (login, haslo, id_adres, data_rejestracji, email, nr_telefonu, imie, nazwisko, rodzaj_konta) values ('admin', 'admin', 1, '2016-10-19 11:34:40', 'email', '000-00-00', 'Sebastian', 'Admin', 2)
GO

create TRIGGER dodawanie_wypozyczenia ON wypozyczenia
FOR INSERT
AS
BEGIN
	DECLARE @przedmiot_id DECIMAL(5,0)
	DECLARE @konto_id DECIMAL(5,0)
	DECLARE @liczba_dostepnych_przedmiotow DECIMAL(4,0) 
	DECLARE @liczba_przedmiotow_wypozyczanych DECIMAL(4,0)

	 
	SELECT @przedmiot_id = inserted.id_przedmiot from inserted
	SELECT @konto_id = inserted.id_konto from inserted
	SELECT @liczba_przedmiotow_wypozyczanych = inserted.liczba_przedmiotow from inserted

	IF EXISTS (SELECT * FROM rezerwacje WHERE id_przedmiot=@przedmiot_id and id_konto=@konto_id)
	BEGIN
	   DELETE FROM rezerwacje where id_przedmiot = @przedmiot_id AND id_konto=@konto_id; 
	END;
	   
	SELECT @liczba_dostepnych_przedmiotow = 
	   (select liczba_dostepnych_przedmiotow from przedmioty where id_przedmiot=@przedmiot_id)

	UPDATE Przedmioty
		  set liczba_dostepnych_przedmiotow = @liczba_dostepnych_przedmiotow - @liczba_przedmiotow_wypozyczanych
		  from przedmioty where id_przedmiot=@przedmiot_id
		  PRINT 'dodano wypozyczenie'
END;
GO

create TRIGGER tworzenie_rezerwacji ON Rezerwacje
AFTER INSERT
AS
BEGIN
	DECLARE @liczba_dost_przed DECIMAL(4,0)
	DECLARE @liczba_przed DECIMAL(4,0) 
	DECLARE @new_id DECIMAL(5,0)
	SELECT @liczba_przed = INSERTED.liczba_przedmiotow from INSERTED
	SELECT @new_id = inserted.id_przedmiot from inserted
	SELECT @liczba_dost_przed = (select liczba_dostepnych_przedmiotow from przedmioty where id_przedmiot=@new_id)
		IF @liczba_dost_przed>=@liczba_przed
		BEGIN
		  UPDATE Rezerwacje
			set data_rezerwacji = GETDATE()
			FROM Rezerwacje
			WHERE id_przedmiot = @new_id
		  UPDATE Przedmioty
			 set liczba_dostepnych_przedmiotow = @liczba_dost_przed - @liczba_przed
			 from przedmioty where id_przedmiot=@new_id
			PRINT 'aktywowano rezerwacje'
		END
END;
GO

create TRIGGER usuwanie_rezerwacji ON Rezerwacje
AFTER Delete
AS
BEGIN
	DECLARE @liczba_dostepnych_przedmiotow DECIMAL(4,0) 
	DECLARE @liczba_przedmiotow_rezerwowanych DECIMAL(4,0) 
	DECLARE @przedmiot_id DECIMAL(5,0)
	DECLARE @konto_id DECIMAL(5,0)
	
	SELECT @konto_id = deleted.id_konto from deleted
	SELECT @liczba_dostepnych_przedmiotow = deleted.liczba_przedmiotow from deleted
	SELECT @przedmiot_id = deleted.id_przedmiot from deleted
	SELECT @liczba_przedmiotow_rezerwowanych = 
	   (select liczba_dostepnych_przedmiotow from przedmioty where id_przedmiot=@przedmiot_id)
	
	IF (SELECT data_rezerwacji FROM DELETED WHERE id_przedmiot=@przedmiot_id and id_konto=@konto_id) is NOT NULL
	BEGIN
	   UPDATE Przedmioty
		  set liczba_dostepnych_przedmiotow = @liczba_dostepnych_przedmiotow + @liczba_przedmiotow_rezerwowanych
		  from przedmioty where id_przedmiot=@przedmiot_id
		  PRINT 'usunieto rezerwacje'
	   END;
END;
GO

CREATE TRIGGER usuwanie_wypozyczenia ON wypozyczenia
AFTER DELETE
AS
BEGIN
	DECLARE @przedmiot_id DECIMAL(5,0)
	DECLARE @konto_id DECIMAL(5,0)
	DECLARE @liczba_dostepnych_przedmiotow DECIMAL(4,0) 
	DECLARE @liczba_przedmiotow_wypozyczanych DECIMAL(4,0)

	 
	SELECT @przedmiot_id = id_przedmiot from deleted
	SELECT @konto_id = id_konto from deleted
	SELECT @liczba_przedmiotow_wypozyczanych = liczba_przedmiotow from deleted
	print '@liczba_przedmiotow_wypozyczanych'
	print @liczba_przedmiotow_wypozyczanych
	IF EXISTS (SELECT * FROM rezerwacje WHERE id_przedmiot=@przedmiot_id and id_konto=@konto_id)
	BEGIN
	   DELETE FROM rezerwacje where id_przedmiot = @przedmiot_id AND id_konto=@konto_id; 
	END;
	   
	SELECT @liczba_dostepnych_przedmiotow = 
	   (select liczba_dostepnych_przedmiotow from przedmioty where id_przedmiot=@przedmiot_id)

	print '@liczba_dostepnych_przedmiotow'
	print @liczba_dostepnych_przedmiotow

	UPDATE Przedmioty
		  set liczba_dostepnych_przedmiotow = @liczba_dostepnych_przedmiotow + @liczba_przedmiotow_wypozyczanych
		  from przedmioty where id_przedmiot=@przedmiot_id
		  PRINT 'usunieto wypozyczenie'
END